//
//  ProfilePersonalSectionCell.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 13/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class ProfilePersonalSectionCell: BaseTableViewCell {
    
    var borderColor = UIColor.white.cgColor
    
    override func getCustomView() -> UIView {
        let view = RightIconTwoLabelView()
        view.tag = 1
        return view
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let separator = UIGraphicsGetCurrentContext()!
        separator.setLineWidth(0.34)
        separator.setStrokeColor(borderColor)
        separator.move(to: CGPoint(x: 0, y: rect.height))
        separator.addLine(to: CGPoint(x: rect.width, y: rect.height))
        separator.strokePath()
    }
    
    override func getLeadingConstraint() -> CGFloat? {
        return 20
    }
    
    override func getTrailingConstraint() -> CGFloat? {
        return 13
    }
    
    override func getTopConstraint() -> CGFloat? {
        return 20
    }
    
    override func getBottomConstraint() -> CGFloat? {
        return 20
    }
}
