//
//  ProfileTableView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class ProfileTableView: BasicTableView {
    
    private var menus = [MainMenuProtocol]()
    var onSelectMenu: (( _ selected: SubMenu) -> Void)?
    
    override func initView() {
        super.initView()
        setMenus()
        separatorStyle = .none
        backgroundColor = UIColor.gray.withAlphaComponent(0.1)
        register(RightIconTwoLabelViewCell.self, forCellReuseIdentifier: RightIconTwoLabelViewCell.identifier)
        register(RightButtonTwoLabelViewCell.self, forCellReuseIdentifier: RightButtonTwoLabelViewCell.identifier)
        
        register(ProfilePersonalSectionCell.self, forCellReuseIdentifier: ProfilePersonalSectionCell.identifier)
        register(ProfileAddressSectionCell.self, forCellReuseIdentifier: ProfileAddressSectionCell.identifier)
    }
    
    private func setMenus() {
        let subMenuPersonal = [SubMenu(topValue: "Avendi Sianipar", bottomValue: nil,
                                       leftIcon: "spiderman", rightIcon: "arrowRight", type: .other),
                               SubMenu(topValue: "Berganti Menjadi Penumpang", bottomValue: nil,
                                       leftIcon: "changeAccount", rightIcon: "arrowRight", type: .other),
                               SubMenu(topValue: "Pencarian Tersimpan", bottomValue: nil,
                                       leftIcon: "bookmark", rightIcon: "arrowRight", type: .other),
                               SubMenu(topValue: "Atur Kendaraan", bottomValue: nil,
                                       leftIcon: "car", rightIcon: "arrowRight", type: .manageVehicle)]
        
        let subMenuAddress = [SubMenu(topValue: "Rumah", bottomValue: "Belum diisi",
                                      leftIcon: "home", rightIcon: "nil", type: .homeAddress),
                              SubMenu(topValue: "Kantor", bottomValue: "Ruko Bidex Blok F20, Jl. Pahlawan Seribu, Lengkong Gudang",
                                      leftIcon: "office", rightIcon: "nil", type: .officeAddress),
                              SubMenu(topValue: "Kampung Halmaan", bottomValue: "Klp. Dua, Kec. Klp. Dua, Tangerang",
                                      leftIcon: "village", rightIcon: "nil", type: .hometownAddress)]
        
        let subMenuProfile = [SubMenu(topValue: "Ubah Profil", bottomValue: "",
                                      leftIcon: "identity", rightIcon: "arrowRight", type: .editProfile),
                              SubMenu(topValue: "Ubah Kata Sandi", bottomValue: "",
                                      leftIcon: "socialMedia", rightIcon: "arrowRight", type: .other),
                              SubMenu(topValue: "Verifikasi Identitas", bottomValue: nil,
                                      leftIcon: "identity", rightIcon: "arrowRight", type: .other),
                              SubMenu(topValue: "Sosial Media", bottomValue: nil,
                                      leftIcon: "socialMedia", rightIcon: "arrowRight", type: .other),]
        
        let subMenuOther = [SubMenu(topValue: "Bantuan Tebengan", bottomValue: nil,
                                    leftIcon: "faq", rightIcon: "arrowRight", type: .other)]
        
        menus.append(PersonalSection(subMenu: subMenuPersonal))
        menus.append(AddressSection(subMenu: subMenuAddress))
        menus.append(ProfileSection(subMenu: subMenuProfile))
        menus.append(OtherSection(subMenu: subMenuOther))
    }
    
    override func getBasicSectionCount() -> Int {
        return menus.count
    }
    
    override func getBasicRowCount(section: Int) -> Int? {
        return menus[section].rowCount
    }
    
    override func getBasicHeaderHeight(section: Int) -> CGFloat {
        return 50
    }
    
    override func getBasicHeaderView(section: Int) -> UIView? {
        if let title = menus[section].sectionTitle {
            let view = OneLineLabelView()
            view.setData(title: title)
            return view
        }
        return nil
    }
    
    override func getBasicCell(indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let menu = menus[indexPath.section]
        
        switch menu.type {
        case .personal:
            if let personalCell = dequeueReusableCell(withIdentifier: ProfilePersonalSectionCell.identifier, for: indexPath) as? ProfilePersonalSectionCell, let view = personalCell.viewWithTag(1) as? RightIconTwoLabelView, let section = menu as? PersonalSection {
                let data = section.subMenu[indexPath.row]
                if indexPath.row == 0 {
                    view.setViewPersonalSection(true)
                    view.setData(data.topValue, "Penumpang", data.leftIcon, data.rightIcon)
                    personalCell.borderColor = UIColor.white.cgColor
                } else {
                    view.setViewPersonalSection(false)
                    view.setData(data.topValue, nil, data.leftIcon, data.rightIcon)
                    personalCell.borderColor = UIColor.gray.cgColor
                }
                view.tappable { [weak self] (_) in
                    self?.onSelectMenu?(data)
                }
                cell = personalCell
            }
            
        case .address:
            cell = dequeueReusableCell(withIdentifier: ProfileAddressSectionCell.identifier, for: indexPath)
            if let view = cell.viewWithTag(1) as? RightButtonTwoLabelView,
                let section = menu as? AddressSection {
                let data = section.subMenu[indexPath.row]
                view.setData(data.topValue, data.bottomValue, data.leftIcon)
                view.setAddressSection(isAlreadySet: data.bottomValue != "Belum diisi")
                view.tappable { [weak self] (_) in
                    self?.onSelectMenu?(data)
                }
            }
        case .profileMenu:
            cell = dequeueReusableCell(withIdentifier: RightIconTwoLabelViewCell.identifier, for: indexPath)
            if let view = cell.viewWithTag(1) as? RightIconTwoLabelView,
                let section = menu as? ProfileSection {
                let data = section.subMenu[indexPath.row]
                view.setOneLabel(topLabel: data.topValue, leftIcon: data.leftIcon, rightIcon: data.rightIcon!)
                view.tappable { [weak self] (_) in
                    self?.onSelectMenu?(data)
                }
            }
        case .other:
            cell = dequeueReusableCell(withIdentifier: RightIconTwoLabelViewCell.identifier, for: indexPath)
            if let view = cell.viewWithTag(1) as? RightIconTwoLabelView,
                let section = menu as? OtherSection {
                let data = section.subMenu[indexPath.row]
                view.setOneLabel(topLabel: data.topValue, leftIcon: data.leftIcon, rightIcon: data.rightIcon ?? "")
            }
        }
        return cell
    }    
}
