//
//  ProfileViewController.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    @IBOutlet private weak var tableView: ProfileTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleSelectedMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = Color.blue
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
    
    func handleSelectedMenu() {
        tableView.onSelectMenu = { [weak self] (data) in
            switch data.type {
            case .editProfile:
                self?.toEditProfile()
            case .homeAddress:
                self?.toSearchPlace()
            case .officeAddress:
                self?.toSearchPlace()
            case .hometownAddress:
                self?.toSearchPlace()
            case .manageVehicle:
                self?.toListVehicle()
            default:
                print("Other")
            }
        }
    }

    func toEditProfile() {
        let controller = EditProfileViewController.instantiate(appStoryboard: .EditProfile)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func toListVehicle() {
        let controller = ListVehicleViewController.instantiate(appStoryboard: .Vehicle)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func toSearchPlace() {
        let controller = SearchAddressViewController.instantiate(appStoryboard: .SearchAddress)
        controller.eventDelegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension ProfileViewController: EventDelegate {
    func triggerEvent(_ type: EventDelegateType) {
        switch type {
        case .choose(let model):
            guard let googlePlace = model as? GooglePlaceDetailModel else { return }
            presentAlertOKHandler(title: googlePlace.structured_formatting.main_text , message: googlePlace.description)
        }
    }
}
