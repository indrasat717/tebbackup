//
//  ProfileMainMenuProtocol.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 10/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation


protocol MainMenuProtocol {
    var type: MainMenu { get }
    var sectionTitle: String? { get }
    var rowCount: Int { get }
}

enum MainMenu {
    case personal
    case address
    case profileMenu
    case other
}

struct SubMenu {
    let topValue: String
    let bottomValue: String?
    let leftIcon: String
    let rightIcon: String?
    let type: SubMenuType
    
    enum SubMenuType {
        case editProfile
        case homeAddress
        case officeAddress
        case hometownAddress
        case manageVehicle
        case other
    }
}

class PersonalSection: MainMenuProtocol {
    var type: MainMenu {
        return .personal
    }
    
    var sectionTitle: String? {
        return "Personal"
    }
    
    var rowCount: Int {
        return subMenu.count
    }
    
    var subMenu: [SubMenu]
    
    init(subMenu: [SubMenu]) {
        self.subMenu = subMenu
    }
}

class AddressSection: MainMenuProtocol {
    var type: MainMenu {
        return .address
    }
    
    var sectionTitle: String? {
        return "Atur Alamat"
    }
    
    var rowCount: Int {
        return subMenu.count
    }
    
    var subMenu: [SubMenu]
    
    init(subMenu: [SubMenu]) {
        self.subMenu = subMenu
    }
}

class ProfileSection: MainMenuProtocol {
    var type: MainMenu {
        return .profileMenu
    }
    
    var sectionTitle: String? {
        return "Menu Profil"
    }
    
    var rowCount: Int {
        return subMenu.count
    }
    
    var subMenu: [SubMenu]
    
    init(subMenu: [SubMenu]) {
        self.subMenu = subMenu
    }
}

class OtherSection: MainMenuProtocol {
    var type: MainMenu {
        return .other
    }
    
    var sectionTitle: String? {
        return "Lainnya"
    }
    
    var rowCount: Int {
        return subMenu.count
    }
    
    var subMenu: [SubMenu]
    
    init(subMenu: [SubMenu]) {
        self.subMenu = subMenu
    }
}
