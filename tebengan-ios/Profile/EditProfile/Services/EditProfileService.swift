//
//  EditProfileService.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 16/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Moya

enum EditProfileService {
    case getUserDetail(userId: Int)
    case editProfile(user: User)
}

// MARK: - TargetType Protocol Implementation
extension EditProfileService: TargetType {
    var baseURL: URL { return App.tebenganHost }
    var path: String {
        switch self {
            case .getUserDetail(let userId):
                return "/UserModels/\(userId)"
        case .editProfile(let user):
            return "/UserModels/\(Int(user.id!))"
        }
        
        
    }
    var method: Moya.Method {
        switch self {
            case .getUserDetail:
                return .get
            case .editProfile:
                return .put
        }
    }
    var task: Task {
        switch self {
            case let .getUserDetail(userId): // Always send parameters as JSON in request body
                var params: [String: Any] = [:]
                params["access_token"] =  UserDefaults.standard.object(forKey: "token") as! String
                return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            case .editProfile(let user):
                var params: [String: Any] = [:]
                var body: [String: Any] = [:]
                params["access_token"] =  UserDefaults.standard.object(forKey: "token") as! String
                body["name"] = user.name
                body["familyName"] = user.familyName
                body["genderId"] = user.genderId
                body["email"] = user.email
                body["company"] = user.company
                body["position"] = user.position
                body["description"] = user.position
                return .requestCompositeParameters(bodyParameters: body, bodyEncoding: JSONEncoding.default, urlParameters: params)
        }
    }
    var sampleData: Data {
        switch self {
                case .getUserDetail:
                    return Data()
                case .editProfile(let user):
                     return "{\"id\":100, \"name\": \(user.name), \"familyName\": \(user.familyName), \"email\":\(user.email), \"genderId\":\(user.genderId), \"password\": \(user.password), \"phoneNumber\": \(user.phoneNumber)\"}".utf8Encoded
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
