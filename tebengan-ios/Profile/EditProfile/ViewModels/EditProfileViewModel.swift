//
//  EditProfileViewModel.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 16/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import RxCocoa
import RxSwift
import Moya

class EditProfileViewModel: ViewModelProtocol {
    struct Input {
        let name: AnyObserver<String>
        let familyName: AnyObserver<String>
        let email:  AnyObserver<String>
        let genderId: AnyObserver<Int>
        let company: AnyObserver<String>
        let position: AnyObserver<String>
        let description: AnyObserver<String>
        let submitDidTap: AnyObserver<Void>
    }
    
    struct Output {
        let userDetailObservable : Observable<UserDetailModel>
        let editProfileObservable : Observable<Response>
        let isLoading: Driver<Bool>
        let isButtonEnable: Driver<Bool>
        let errorObservable : Observable<Error>
    }
    
    let input: Input
    let output: Output
    
    private let disposeBag = DisposeBag()

    private let provider = MoyaProvider<EditProfileService>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
    private let nameSubject = PublishSubject<String>()
    private let familyNameSubject = PublishSubject<String>()
    private let emailSubject = PublishSubject<String>()
    private let genderIdSubject = PublishSubject<Int>()
    private let companySubject = PublishSubject<String>()
    private let positionSubject = PublishSubject<String>()
    private let descriptionSubject = PublishSubject<String>()
    
    private let userDetailSubject = PublishSubject<UserDetailModel>()
    
    private let submitDidTapSubject = PublishSubject<Void>()
    private let editProfilerResultSubject = PublishSubject<Response>()
    private let errorsSubject = PublishSubject<Error>()
    private let isLoadingSubject = PublishSubject<Bool>()
    private let isButtonEnableSubject = PublishSubject<Bool>()
    private let userId = UserDefaults.standard.object(forKey: "userId") as! Int
    
    private var userObservable : Observable<User>{

        return Observable.combineLatest(
            nameSubject.asObservable(),
            familyNameSubject.asObservable(),
            emailSubject.asObservable(),
            genderIdSubject.asObservable(),
            companySubject.asObservable(),
            positionSubject.asObservable(),
            descriptionSubject.asObservable()
        ){
            (name, familyName, email, genderId ,  company, position, description) in
            return User(id:self.userId, name: name, familyName: familyName, genderId: genderId, email: email,  company: company, position: position, description: description)
        }
    }
    
    init(){
        input = Input(name: nameSubject.asObserver(), familyName: familyNameSubject.asObserver(), email: emailSubject.asObserver(), genderId: genderIdSubject.asObserver(), company: companySubject.asObserver(), position: positionSubject.asObserver(), description: descriptionSubject.asObserver(), submitDidTap: submitDidTapSubject.asObserver())
        
        output = Output(userDetailObservable: userDetailSubject.asObservable(), editProfileObservable: editProfilerResultSubject.asObservable(), isLoading: isLoadingSubject.asDriver(onErrorJustReturn: false), isButtonEnable: isLoadingSubject.asDriver(onErrorJustReturn: false), errorObservable: errorsSubject.asObservable())
        
        getUserDetailService()
        submit()
    }
    
    private func submit(){
        submitDidTapSubject
        .do(onNext: { [unowned self] _ in
            self.isLoadingSubject.onNext(true)
        })
        .withLatestFrom(userObservable)
        .flatMapLatest{user in
            return self.changeProfileService(user: user).materialize()
        }
        .do(onNext: { [unowned self] _ in
            self.isLoadingSubject.onNext(false)
        })
        .subscribe(onNext: { [weak self] event in
            switch event {
            case .next(let response):
                print("success")
                break
            case .error(let error):
                print("error \(error.localizedDescription)")
                break
            default:
                break
            }
        })
        .disposed(by: disposeBag)
    }
    
    private func getUserDetailService(){
        provider.rx
            .request(.getUserDetail(userId: userId))
            .asObservable()
            .map(UserDetailModel.self)
            .subscribe(userDetailSubject)
            .disposed(by: disposeBag)
    }
    
    private func changeProfileService(user: User) -> Observable<Response>{
        return provider.rx
                .request(.editProfile(user: user))
                .filterSuccessfulStatusCodes()
                .asObservable()
        
    }
    
    deinit {
              print("\(self) dealloc")
       }
    
}
