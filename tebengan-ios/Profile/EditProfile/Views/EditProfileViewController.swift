//
//  EditProfileViewController.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 13/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class EditProfileViewController: BaseViewController, ControllerType {

    typealias ViewModelType = EditProfileViewModel
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()


    @IBOutlet weak var headerView: EditProfileHeaderView!
    @IBOutlet weak var editProfileView: EditProfileView!
    
    weak var eventDelegate: EventDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let viewModel = EditProfileViewModel()
        configure(with: viewModel)
        addChangeProfileAction()
    }
    
    private func addChangeProfileAction(){
        headerView.changeAvatarButton.addTarget(self, action: #selector(pickImage), for: .touchUpInside)
    }
    
    func configure(with viewModel: EditProfileViewModel) {
        
            
        viewModel.output.userDetailObservable.subscribe(onNext: { [unowned self] user in
            //Header profile form
            let url = URL(string: user.avatarUrl ?? "")
            if url != nil {
                let data = try? Data(contentsOf: url!)
                self.headerView.imageAvatar.image = UIImage(data: data!)
            }else{
                self.headerView.imageAvatar.image = UIImage(named: "spiderman")
            }
        
            self.headerView.nameTextField.text = user.name ?? ""
            self.headerView.familyNameTextField.text = user.familyName ?? ""
            self.headerView.genderView.selectedIndex = user.genderId ?? 0
            
            //Main profile form
            self.editProfileView.emailTextField.text = user.email ?? ""
            self.editProfileView.companyTextField.text = user.company ?? ""
            self.editProfileView.positionTextField.text = user.position ?? ""
            self.editProfileView.descriptionTextField.text = user.description ?? ""
            }).disposed(by: disposeBag)
        
        headerView.nameTextField.textObs.orEmpty.asObservable()
            .subscribe(viewModel.input.name)
            .disposed(by: disposeBag)
        
        headerView.familyNameTextField.textObs.orEmpty.asObservable()
            .subscribe(viewModel.input.familyName)
            .disposed(by: disposeBag)
        
        let selectedGender = Observable<Int>.just(headerView.genderView.selectedIndex)
        selectedGender
            .subscribe(viewModel.input.genderId)
            .disposed(by: disposeBag)
        
        editProfileView.emailTextField.textObs.orEmpty.asObservable()
            .subscribe(viewModel.input.email)
            .disposed(by: disposeBag)
        
        editProfileView.companyTextField.textObs.orEmpty.asObservable()
            .subscribe(viewModel.input.company)
            .disposed(by: disposeBag)
        
        editProfileView.positionTextField.textObs.orEmpty.asObservable()
            .subscribe(viewModel.input.position)
            .disposed(by: disposeBag)
        
        editProfileView.descriptionTextField.textObs.orEmpty.asObservable()
            .subscribe(viewModel.input.description)
            .disposed(by: disposeBag)
        
        editProfileView.submitButton.rx.tap.asObservable()
            .subscribe(viewModel.input.submitDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.isLoading.drive(onNext: {[unowned self] isLoading in
            self.showLoading(isLoading)
        }).disposed(by: disposeBag)
    }
    
    
    @objc private func pickImage(_ sender: Any){
        ImagePickerManager().pickImage(self){ (image, URL) in
            //here is the image
            self.headerView.imageAvatar.image = image
        }

    }
    
    @objc func imageTapped(sender: UITapGestureRecognizer){
           let imageView = sender.view as! UIImageView
        
           let newImageView = UIImageView(image: imageView.image)
           newImageView.frame = UIScreen.main.bounds
           newImageView.backgroundColor = .black
           newImageView.contentMode = .scaleAspectFit
           newImageView.isUserInteractionEnabled = true
        
           let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(sender:)))
           newImageView.addGestureRecognizer(tap)
        
           self.view.addSubview(newImageView)
           self.navigationController?.isNavigationBarHidden = true
           self.tabBarController?.tabBar.isHidden = true
           
       }
       

      @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
           self.navigationController?.isNavigationBarHidden = false
           self.tabBarController?.tabBar.isHidden = false
           sender.view?.removeFromSuperview()
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
