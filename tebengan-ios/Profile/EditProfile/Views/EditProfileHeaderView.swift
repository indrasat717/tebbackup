//
//  EditProfileHeaderView.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 13/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class EditProfileHeaderView: XIBView {
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var nameTextField: TextFieldInputView!
    @IBOutlet weak var familyNameTextField: TextFieldInputView!
    @IBOutlet weak var genderView: GenderView!
    @IBOutlet weak var changeAvatarButton: UIButton!
    
    
    override func initView() {
        setupUI()
    }
    
    private func setupUI(){
        imageAvatar.layer.borderWidth = 1
        imageAvatar.layer.masksToBounds = false
        imageAvatar.layer.cornerRadius = imageAvatar.frame.height/2
        imageAvatar.clipsToBounds = true
    }
        
    
    
}
