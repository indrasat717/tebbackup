//
//  EditProfileView.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 13/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class EditProfileView: XIBView {

    @IBOutlet weak var emailTextField: TextFieldInputView!
    @IBOutlet weak var companyTextField: TextFieldInputView!
    @IBOutlet weak var positionTextField: TextFieldInputView!
    @IBOutlet weak var descriptionTextField: TextFieldInputView!
    @IBOutlet weak var submitButton: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
