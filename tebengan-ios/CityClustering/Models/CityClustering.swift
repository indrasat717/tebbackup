//
//  CityClustering.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 16/10/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import Foundation
struct CityClustering{
    var id: Int
    var title: String
    var description: String
    var image: String
}
