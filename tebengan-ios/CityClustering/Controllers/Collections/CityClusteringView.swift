//
//  CityClusteringView.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 30/01/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
@IBDesignable
class CityClusteringView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    private let reuseIdentifier = "CityClusteringCellView"
    private let xibName = "CityClusteringCellView"
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dummyCityClusterings = [
        CityClustering(id: 1, title: "12+ Tebengan", description: "Perjalanan jauh terasa dekat dengan menggunakan tebengan", image: "CityClustering"),
        CityClustering(id: 2, title: "16+ Tebengan", description: "Perjalanan jauh terasa dekat dengan menggunakan tebengan", image: "CityClustering")
    ]
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let bundle = Bundle(for: type(of: self))
        bundle.loadNibNamed("CityClusteringView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        initCollectionView()
    }
    
    private func initCollectionView() {
        let nib = UINib(nibName: xibName, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.dataSource = self
    }


}

extension CityClusteringView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dummyCityClusterings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CityClusteringCellView else {
            fatalError("can't dequeue CustomCell")
        }
        // Configure the cell
        let dummyCityClustering = dummyCityClusterings[indexPath.row]
        cell.setup(data: dummyCityClustering)
        return cell
    }

}
