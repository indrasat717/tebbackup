//
//  CityClusteringCollectionViewCell.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 16/10/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import UIKit

protocol CityClusteringCell {
    func setup(data: CityClustering)
}
class CityClusteringCellView: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var switchButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    public func setup(data: CityClustering){
        self.titleLabel.text = data.title
        self.descriptionLabel.text = data.description
        self.image.image = UIImage(named: data.image)
    }
    
}
