//
//  MainHeaderView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 3/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class MainHeaderView: XIBView {
    
    @IBOutlet private weak var cardView: UIView!
    @IBOutlet private weak var tebenganIcon: UIImageView!
    @IBOutlet private weak var locationView: UIView!
    @IBOutlet private weak var searchView: TappableView!
    @IBOutlet private weak var cardViewHover: UIView!
    @IBOutlet private weak var searchViewHover: UIView!

    override func initView() {
        searchView.layer.cornerRadius = 10
        locationView.layer.cornerRadius = 20
        
        cardViewHover.alpha = 0
        searchViewHover.alpha = 0
        searchViewHover.layer.cornerRadius = 10
        
        searchView.tappable { (_) in
            print("searchview tapped")
        }
    }
    
    func setBackgroundClear() {
        cardView.backgroundColor = .clear
    }
    
    func setBackgroundImage() {
        cardView.layer.contents = UIImage(named: "HeaderBackground")!.cgImage
    }

    func isCardViewItemHidden(isHidden: Bool) {
        tebenganIcon.isHidden = isHidden
        locationView.isHidden = isHidden
        searchView.isHidden = isHidden
    }
    
    func setSearchViewHover(alpha: CGFloat) {
        searchViewHover.alpha = alpha
        cardViewHover.alpha = alpha
        cardViewHover.backgroundColor = Color.blue
    }
}
