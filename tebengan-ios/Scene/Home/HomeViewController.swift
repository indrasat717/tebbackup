//
//  HomeViewController.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 23/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var mainStackView: UIStackView!
    @IBOutlet private weak var mainHeaderViewBack: MainHeaderView!
    @IBOutlet private weak var mainHeaderViewFront: MainHeaderView!
    
    private var gestureSwipe: ScrollViewGestureSwipe = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        mainHeaderViewBack.setBackgroundImage()
        mainHeaderViewFront.setBackgroundClear()
        
        mainHeaderViewFront.tappable { (_) in
            let storyBoard = UIStoryboard(name: "Login", bundle:nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "initial")
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: 0, y: mainHeaderViewBack.bounds.height,
                           width: view.bounds.width,
                           height: view.bounds.height * 2)
        mainStackView.addBackgroundView(color: .white, frame: frame)
    }
}

private extension HomeViewController {
    func checkComponentHeaderHover() {
        let contentOffset = (scrollView.contentOffset.y) / 80
        mainHeaderViewBack.setSearchViewHover(alpha: contentOffset)
        UIApplication.statusBarBackgroundColor = Color.blue.withAlphaComponent(contentOffset)
    }
    
    func collapseScrollView(gestureSwipe: ScrollViewGestureSwipe) {
        UIView.animate(withDuration: 0.3) {
            self.scrollView.contentOffset.y = gestureSwipe == .downToTop ? 85 : -44
        }
    }    
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        checkComponentHeaderHover()
        if scrollView.contentOffset.y != -44 {
            self.mainHeaderViewFront.isCardViewItemHidden(isHidden: true)
        } else {
            if gestureSwipe == .topToDown {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.mainHeaderViewFront.isCardViewItemHidden(isHidden: false)
                }
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        if translation.y > 0 {
            gestureSwipe = .topToDown
        } else {
            gestureSwipe = .downToTop
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if -44 ... 85 ~= scrollView.contentOffset.y {
            collapseScrollView(gestureSwipe: gestureSwipe)
        }
    }
}
