//
//  ChatTableView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class ChatTableView: BasicTableView {
    
    override func initView() {
        super.initView()
        register(ChatViewCell.self, forCellReuseIdentifier: ChatViewCell.identifier)
    }
    
    override func getBasicRowCount(section: Int) -> Int? {
        return 3
    }
    
    override func getBasicCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: ChatViewCell.identifier, for: indexPath)
        return cell
    }
    
}
