//
//  ChatViewCell.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class ChatViewCell: BaseTableViewCell {
    override func getCustomView() -> UIView {
        let view = ChatItemView()
        view.tag = 1
        return view
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let separator = UIGraphicsGetCurrentContext()!
        separator.setLineWidth(0.3)
        separator.setStrokeColor(UIColor.gray.cgColor)
        separator.move(to: CGPoint(x: 0, y: rect.height))
        separator.addLine(to: CGPoint(x: rect.width, y: rect.height))
        separator.strokePath()
    }
}
