//
//  CityClusteringCollectionViewCell.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 16/10/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import UIKit

class CityClusteringCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var switchButton: UIButton!
    
}
