//
//  MyService.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 19/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation
import Moya

enum TebenganService {
    case createUser(user: User)
    case requestOTP(code: String, number: String)
    case confirmPIN(pin: String)
    case confirmEmail
}

// MARK: - TargetType Protocol Implementation
extension TebenganService: TargetType {
    var baseURL: URL { return App.tebenganHost }
    var path: String {
        switch self {
            case .createUser:
                return "/UserModels"
            case .requestOTP:
                return "/UserModels/verification/phone/send"
            case .confirmPIN:
                return "/UserModels/verification/phone/confirmation"
            case .confirmEmail:
                return "/UserModels/_resendEmailVerification"
        }
        
        
    }
    var method: Moya.Method {
        switch self {
            case .createUser:
                return .post
            case .requestOTP:
                return .post
            case .confirmPIN:
                return .post
            case .confirmEmail:
                return .get
        }
    }
    var task: Task {
        switch self {
            case let .createUser(user): // Always send parameters as JSON in request body
                return .requestJSONEncodable(user)
            case let .requestOTP(code, number):
                var params: [String: Any] = [:]
                params["access_token"] = UserDefaults.standard.object(forKey: "token") as! String
                params["code"] = code
                params["number"] = number
                return  .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            case let .confirmPIN(pin):
                var params: [String: Any] = [:]
                params["access_token"] = UserDefaults.standard.object(forKey: "token") as! String
                params["pin"] = pin
                return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            case let .confirmEmail:
                var params: [String: Any] = [:]
                params["access_token"] = UserDefaults.standard.object(forKey: "token") as! String
                return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }
    var sampleData: Data {
        switch self {
                case .createUser(let user):
                    return "{\"id\":100, \"name\": \(user.name), \"familyName\": \(user.familyName), \"email\":\(user.email), \"genderId\":\(user.genderId), \"password\": \(user.password), \"phoneNumber\": \(user.phoneNumber)\"}".utf8Encoded
                case .requestOTP(let code, let number):
                    return "{\"code\":\(code), \"number\":\(number)}".utf8Encoded
                case .confirmPIN(let pin):
                    return "{\"pin\":\(pin)}".utf8Encoded
                case .confirmEmail:
                    return "".utf8Encoded
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
