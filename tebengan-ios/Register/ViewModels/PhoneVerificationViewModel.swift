//
//  PhoneVerificationViewModel.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 27/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

class PhoneVerificationViewModel: ViewModelProtocol {
    //Timer count down duration
    private let duration = 120
    //Country code
    private let code: String = "62"
    
    
    struct Input {
        let pin: AnyObserver<String>
        let submitButtonDidTap: AnyObserver<Void>

    }
    
    struct Output {
        let pinConfirmationObservable : Observable<Response>
        let countDownTimerObservable : Observable<String>
        let isLoading : Driver<Bool>
        let errorObservable : Observable<Error>
    }
    
    let input: Input
    let output: Output
    
    private let disposeBag = DisposeBag()
    
    private let provider = MoyaProvider<TebenganService>()
    
    private let numberSubject = PublishSubject<String>()
    private let pinSubject = PublishSubject<String>()
    private let pinConfirmationResultSubject = PublishSubject<Response>()
    private let countDownTimerSubject = PublishSubject<String>()
    private let submitButtonDidTapSubject = PublishSubject<Void>()
    private let isLoadingSubject = PublishSubject<Bool>()
    private let errorsSubject = PublishSubject<Error>()
    
    init(number: String) {
        input = Input(
            pin: pinSubject.asObserver(),
            submitButtonDidTap: submitButtonDidTapSubject.asObserver()
        )
        
        output = Output(
            pinConfirmationObservable: pinConfirmationResultSubject.asObservable(),
            countDownTimerObservable: countDownTimerSubject.asObservable(),
            isLoading: isLoadingSubject.asDriver(onErrorJustReturn: false),
            errorObservable: errorsSubject.asObservable()
        )
        
        requestOTPService(code, validatePhoneNumber(number))
        confirmPIN()
        
    }
    
    deinit {
           print("\(self) dealloc")
    }
    
    func validatePhoneNumber(_ phoneNumber: String) -> String {
        return String(phoneNumber.dropFirst())
    }
    
    private func countDownTimer(){
        Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        .take(self.duration + 1)
        .map{(self.duration - $0).intToMinutesStringFormat()}
        .bind(to: countDownTimerSubject)
    }
    
    
    private func requestOTPService(_ code: String, _ number: String){
        provider.rx
            .request(.requestOTP(code: code, number: number))
            .filterSuccessfulStatusCodes()
            .subscribe(onSuccess: { response in
                    self.countDownTimer()
                }) { (error) in
                print(error)
            }
            .disposed(by: disposeBag)
    }
    
    private func confirmPIN(){
        submitButtonDidTapSubject
            .do(onNext: { [unowned self] _ in
                self.isLoadingSubject.onNext(true)
            })
            .withLatestFrom(pinSubject.asObservable())
            .flatMapLatest{pin in
                return self.confirmPINService(pin).materialize()
            }.do(onNext: { [unowned self] _ in
                self.isLoadingSubject.onNext(false)
            })
            .subscribe(onNext: { [weak self] event in
                switch event {
                case .next(let response):
                    self?.pinConfirmationResultSubject.onNext(response)
                case .error(let error):
                    self?.errorsSubject.onNext(error)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func confirmPINService(_ pin: String) -> Observable<Response> {
        return provider.rx
            .request(.confirmPIN(pin: pin))
            .filterSuccessfulStatusCodes()
            .asObservable()
    }
}

