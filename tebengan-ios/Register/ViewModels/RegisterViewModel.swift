//
//  RegisterViewModel.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 19/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

class RegisterViewModel: ViewModelProtocol {
    
    struct Input {
        let name: AnyObserver<String>
        let familyName:  AnyObserver<String>
        let email:  AnyObserver<String>
        let gender: AnyObserver<Int>
        let password:  AnyObserver<String>
        let phoneNumber:  AnyObserver<String>
        let signUpDidTap: AnyObserver<Void>
    }
    struct Output {
        let registerResultObservable : Observable<Response>
        let isLoading: Driver<Bool>
        let isButtonEnable: Driver<Bool>
        let errorObservable : Observable<Error>
    }
    
    let input: Input
    let output: Output
    
    private let disposeBag = DisposeBag()

    private let nameSubject = PublishSubject<String>()
    private let familyNameSubject = PublishSubject<String>()
    private let emailSubject = PublishSubject<String>()
    private let genderSubject = PublishSubject<Int>()
    private let passwordSubject = PublishSubject<String>()
    private let phoneNumberSubject = PublishSubject<String>()
    private let signUpDidTapSubject = PublishSubject<Void>()
    private let registerResultSubject = PublishSubject<Response>()
    private let errorsSubject = PublishSubject<Error>()
    private let isLoadingSubject = PublishSubject<Bool>()
    private let isButtonEnableSubject = PublishSubject<Bool>()
    
    private let provider = MoyaProvider<TebenganService>()
    private let loginProvider = MoyaProvider<LoginService>()
    
    private var userObservable : Observable<User>{

        return Observable.combineLatest(
            nameSubject.asObservable(),
            familyNameSubject.asObservable(),
            emailSubject.asObservable(),
            genderSubject.asObservable(),
            passwordSubject.asObservable(),
            phoneNumberSubject.asObservable()
        ){
            (name, familyName, email, genderId , password, phoneNumber) in
                return User(name: name,
                            familyName: familyName,
                            genderId: genderId,
                            email: email,
                            password: password,
                            phoneNumber: phoneNumber)
        }
    }
    
    
    init() {
        input = Input(
            name: nameSubject.asObserver(),
            familyName: familyNameSubject.asObserver(),
            email: emailSubject.asObserver(),
            gender: genderSubject.asObserver(),
            password: passwordSubject.asObserver(),
            phoneNumber: phoneNumberSubject.asObserver(),
            signUpDidTap: signUpDidTapSubject.asObserver())
        
        output = Output(registerResultObservable: registerResultSubject.asObservable(),
                        isLoading: isLoadingSubject.asDriver(onErrorJustReturn: false),
                        isButtonEnable: isLoadingSubject.asDriver(onErrorJustReturn: false),
                        errorObservable: errorsSubject.asObservable())
        formValidation()
        signUpDidTap()
    }
    
    private func formValidation(){
        let nameValidation = nameSubject
            .map({!$0.isEmpty})
            .share(replay: 1)
        
        let familyNameValidation = familyNameSubject
            .map({!$0.isEmpty})
            .share(replay: 1)
        
        let genderValidation = genderSubject
            .map({!String($0).isEmpty})
            .share(replay: 1)
        
        let emailValidation = emailSubject
            .map({!$0.isEmpty})
            .share(replay: 1)
        
        let passwordValidation = passwordSubject
            .map({!$0.isEmpty})
            .share(replay: 1)
        
        let phoneNumberValidation = phoneNumberSubject
            .map({!$0.isEmpty})
            .share(replay: 1)
        
        let enableButton = Observable.combineLatest(
            nameValidation,
            familyNameValidation,
            emailValidation,
            passwordValidation,
            phoneNumberValidation
        ){
            !$0 && !$1 && !$2 && !$3 && !$4
        }
        .share(replay: 1)
        
        enableButton
            .bind(to: isButtonEnableSubject).disposed(by: disposeBag)
    }
    
    private func signUpDidTap(){
        signUpDidTapSubject
            .do(onNext: { [unowned self] _ in
                self.isLoadingSubject.onNext(true)
            })
            .withLatestFrom(userObservable)
            .flatMapLatest{user in
                return self.submitHandler(user).materialize()
            }
            .withLatestFrom(userObservable)
            .flatMapLatest{ user in
                return self.loginService(user).materialize()
            }
            .do(onNext: { [unowned self] _ in
                self.isLoadingSubject.onNext(false)
            })
            .subscribe(onNext: { [weak self] event in
                switch event {
                case .next(let response):
                    self?.storeToken(response)
                    self?.registerResultSubject.onNext(response)
                case .error(let error):
                    self?.errorsSubject.onNext(error)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func submitHandler(_ user: User) -> Observable<Response>{
        return provider.rx
            .request(
                .createUser(user: user))
            .asObservable()
    }
    
    
    private func loginService(_ user: User) -> Observable<Response>{
        return self.loginProvider.rx
            .request(.loginMail(user.email!, user.password!))
                .filterSuccessfulStatusCodes()
                .asObservable()
                
    }
    
    private func storeToken(_ response: Response){
        let data: LoginModel = ResponseHandler.getResponse(data: response.data)!
        UserDefaults.standard.set(data.id, forKey:"token")
        UserDefaults.standard.synchronize()
    }
    
    deinit {
           print("\(self) dealloc")
    }
}
