//
//  EmailVerificationViewModel.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 11/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

class EmailVerficationViewModel: ViewModelProtocol {
    struct Input{
        let resendEmailButtonDidTap: AnyObserver<Void>
    }
    
    struct Output{
        let resendEmailVerificationResultObservable : Observable<Response>
        let isLoading : Driver<Bool>
        let errorObservable : Observable<Error>

    }
    
    let input: Input
    let output: Output
    
    private let disposeBag = DisposeBag()
    
    private let provider = MoyaProvider<TebenganService>()
    
    private let resendEmailButtonDidTapSubject = PublishSubject<Void>()
    private let resendEmailVerificationResultSubject = PublishSubject<Response>()
    private let isLoadingSubject = PublishSubject<Bool>()
    private let errorsSubject = PublishSubject<Error>()
    
    init() {
        input = Input(resendEmailButtonDidTap: resendEmailButtonDidTapSubject.asObserver())
        
        output = Output(resendEmailVerificationResultObservable: resendEmailVerificationResultSubject.asObservable(),
            isLoading: isLoadingSubject.asDriver(onErrorJustReturn: false),
            errorObservable: errorsSubject.asObservable())
        
        resendEmail()
    }
    
    private func resendEmail(){
        resendEmailButtonDidTapSubject
        .do(onNext: { [unowned self] _ in
            self.isLoadingSubject.onNext(true)
        })
        .flatMapLatest{ _ in
            self.resendEmailVerificationService().materialize()
        }
        .do(onNext: { [unowned self] _ in
            self.isLoadingSubject.onNext(false)
        })
        .subscribe(onNext: { [weak self] event in
            switch event {
            case .next(let response):
                self?.resendEmailVerificationResultSubject.onNext(response)
                print("status code : \(response.statusCode)")
            case .error(let error):
                self?.errorsSubject.onNext(error)
            default:
                break
            }
        })
        .disposed(by: disposeBag)
    }
    
    private func resendEmailVerificationService() -> Observable<Response>{
        return provider.rx.request(.confirmEmail)
        .filterSuccessfulStatusCodes()
        .asObservable()
    }
    
}
