//
//  EmailVerificationView.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 25/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class EmailVerificationView: XIBView {
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var resendEmailVerificationButton: UIButton!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
