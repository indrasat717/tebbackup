//
//  RegisterView.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 14/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class RegisterView: XIBView {
    @IBOutlet weak var firstNameTextField: TextFieldInputView!
    @IBOutlet weak var lastNameTextField: TextFieldInputView!
    @IBOutlet weak var genderSegmentControl: GenderView!
    @IBOutlet weak var emailTextField: TextFieldInputView!
    @IBOutlet weak var passwordTextField: TextFieldInputView!
    @IBOutlet weak var phoneNumberTextField: TextFieldInputView!
    @IBOutlet weak var submitButton: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
