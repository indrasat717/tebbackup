//
//  PhoneNumberVerificationViewController.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 26/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PhoneNumberVerificationViewController: BaseViewController, ControllerType {
    
    
    typealias ViewModelType = PhoneVerificationViewModel
    
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()
    
    
    @IBOutlet var phoneNumberVerificationView: PhoneNumberVerificationView!
    
    let otpStackView = OTPStackView()
    var phoneNumber = String()
    var email = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupOTPStackView()
        setupUI()
        
        let phoneVerificationViewModel = PhoneVerificationViewModel(number:phoneNumber)
        
        configure(with: phoneVerificationViewModel)
        
    }
    
    func setupUI(){
        phoneNumberVerificationView.phoneNumberLabel.text = phoneNumber
    }
    
    func setupOTPStackView(){
        phoneNumberVerificationView.OTPInputContainer.addSubview(otpStackView)
        otpStackView.delegate = self
        otpStackView.heightAnchor.constraint(equalTo: phoneNumberVerificationView.OTPInputContainer.heightAnchor).isActive = true
        otpStackView.centerXAnchor.constraint(equalTo: phoneNumberVerificationView.OTPInputContainer.centerXAnchor).isActive = true
        otpStackView.centerYAnchor.constraint(equalTo: phoneNumberVerificationView.OTPInputContainer.centerYAnchor).isActive = true
    }
    
    func configure(with viewModel: PhoneVerificationViewModel) {
        
        let observableOTPText = Observable<String>.just(otpStackView.getOTP())
        
        observableOTPText.subscribe(viewModel.input.pin)
        
        phoneNumberVerificationView.submitButton.rx.tap.asObservable()
            .subscribe(viewModel.input.submitButtonDidTap)
            .disposed(by: disposeBag)
    
        viewModel.output.countDownTimerObservable
                .subscribe(onNext: { [unowned self] time in
                    self.phoneNumberVerificationView.timeCounterLabel.text = time
                })
                .disposed(by: disposeBag)
        
        viewModel.output.isLoading
            .drive(onNext: {[unowned self] isLoading in
                self.showLoading(isLoading)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.pinConfirmationObservable
            .subscribe(onNext: { [unowned self] response in
                self.performSegue(withIdentifier: "showEmailVerification", sender: self)
            })
            .disposed(by: disposeBag)
    }
    

}

extension PhoneNumberVerificationViewController: OTPDelegate {
    func didChangeValidity(isValid: Bool) {
        
    }
    
    
}
