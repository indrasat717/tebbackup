//
//  RegisterViewController.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 14/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RegisterViewController: BaseViewController, ControllerType {
    
    typealias ViewModelType = RegisterViewModel
    
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()

    
    // MARK: - UI
    @IBOutlet weak var registerView: RegisterView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let registerViewModel = RegisterViewModel()
        configure(with: registerViewModel)

        
    }
    
    func configure(with viewModel: ViewModelType) {
        
        registerView.firstNameTextField.textObs.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.name)
            .disposed(by: disposeBag)
            
        registerView.lastNameTextField.textObs.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.familyName)
            .disposed(by: disposeBag)
        
        let selectedGender = Observable<Int>.just(registerView.genderSegmentControl.selectedIndex)
        
        selectedGender
            .subscribe(viewModel.input.gender)
            .disposed(by: disposeBag)
            
        registerView.emailTextField.textObs.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.email)
            .disposed(by: disposeBag)
        
        registerView.passwordTextField.textObs.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.password)
            .disposed(by: disposeBag)
        
        registerView.phoneNumberTextField.textObs.asObservable()
            .ignoreNil()
            .subscribe(viewModel.input.phoneNumber)
            .disposed(by: disposeBag)
        
        registerView.submitButton.rx.tap.asObservable()
            .subscribe(viewModel.input.signUpDidTap)
            .disposed(by: disposeBag)

        viewModel.output.registerResultObservable.subscribe(onNext: {[unowned self] user in
                self.performSegue(withIdentifier: "showPhoneVerification", sender: self)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.isLoading.drive(onNext: {[unowned self] isLoading in
            self.showLoading(isLoading)
        }).disposed(by: disposeBag)
        
        viewModel.output.isButtonEnable.drive(onNext: {[unowned self] isEnable in
            self.registerView.submitButton.enableButton(isEnable)
        }).disposed(by: disposeBag)
            
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoneVerification" {
                let nav = segue.destination as! UINavigationController
                let phoneNumberVerificationViewController = nav.topViewController as! PhoneNumberVerificationViewController
                phoneNumberVerificationViewController.phoneNumber = registerView.phoneNumberTextField.text
        }
    }
    

}

