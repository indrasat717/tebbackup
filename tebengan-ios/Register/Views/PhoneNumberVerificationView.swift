//
//  PhoneNumberVerification.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 26/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class PhoneNumberVerificationView: XIBView {
    @IBOutlet weak var OTPInputContainer: UIView!
    @IBOutlet weak var timeCounterLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
