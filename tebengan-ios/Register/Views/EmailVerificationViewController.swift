//
//  EmailVerificationViewController.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 10/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EmailVerificationViewController: BaseViewController, ControllerType {
    
    typealias ViewModelType = EmailVerficationViewModel
    
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()
    var email = String()
    
    @IBOutlet weak var emailVerificationView: EmailVerificationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let emailVerificationViewModel = EmailVerficationViewModel()
        configure(with: emailVerificationViewModel)
    }
    
    func configure(with viewModel: EmailVerficationViewModel) {
        emailVerificationView.emailLabel.text = email
        
        viewModel.output.resendEmailVerificationResultObservable.subscribe(onNext: {[unowned self] response in
                self.presentMessage("Berhasil Kirim Ulang")
        })
        .disposed(by: disposeBag)
        
        viewModel.output.isLoading.drive(onNext: {[unowned self] isLoading in
            self.showLoading(isLoading)
        }).disposed(by: disposeBag)
        
        emailVerificationView.resendEmailVerificationButton.rx.tap.asObservable()
            .subscribe(viewModel.input.resendEmailButtonDidTap)
            .disposed(by: disposeBag)
    }


}
