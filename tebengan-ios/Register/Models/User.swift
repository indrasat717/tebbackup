//
//  User.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 20/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

struct User: Codable {
    var id: Int?
    var name: String?
    var familyName : String?
    var genderId: Int?
    var email: String?
    var password: String?
    var phoneNumber: String?
    var company : String?
    var position: String?
    var description: String?
}


