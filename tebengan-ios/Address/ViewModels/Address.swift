//
//  Address.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 04/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

public class Address : NSObject {
    public let id: Int
    public let location: String
    public let rideCount: Int
    
    init(
        id: Int,
        location: String,
        rideCount: Int
    ) {
        self.id = id
        self.location = location
        self.rideCount = rideCount
    }
}
