//
//  AddressCellView.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 04/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

protocol AddressCell {
    func setup(data: Address)
}

class AddressCellView: UICollectionViewCell, AddressCell {
    
    @IBOutlet var cardView: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var rideCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        cardView.createCardView()
    }
    
    
    func setup(data: Address) {
        self.locationLabel.text = data.location
        self.rideCountLabel.text = String(data.rideCount)
    }
    
    
    
    
}
