//
//  AddressView.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 04/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
@IBDesignable
class AddressView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var dashedLine1: UIView!
    @IBOutlet weak var dashedLine2: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var moreButton: UIButton!
    
    private let reuseIdentifier = "AddressCellView"
    private let xibName = "AddressCellView"
    
    var dummyAddresses = [
        Address(id: 1, location: "Jakarta Selatan", rideCount: 80),
        Address(id: 2, location: "Jakarta Barat", rideCount: 40),
        Address(id: 3, location: "Kota Tangerang", rideCount: 15),
        Address(id: 4, location: "Bandung", rideCount: 30),
        Address(id: 4, location: "Serang", rideCount: 5),
    ]
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let bundle = Bundle(for: type(of: self))
        bundle.loadNibNamed("AddressView", owner: self, options: nil)
        addSubview(contentView)
        setupContentView()
        createDashedLine()
        initCollectionView()
        initButton()
    }
    
    private func setupContentView(){
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    private func createDashedLine(){
        dashedLine1.createDashedLine(orientation: .horizontal, color: UIColor.darkGray.cgColor)
        dashedLine2.createDashedLine(orientation: .horizontal, color: UIColor.darkGray.cgColor)
    }
    
    private func initCollectionView() {
        let nib = UINib(nibName: xibName, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.dataSource = self
    }
    
    private func initButton(){
        moreButton.layer.borderColor = UIColor.systemBlue.cgColor
    }

}

extension AddressView: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dummyAddresses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? AddressCellView else {
            fatalError("can't dequeue CustomCell")
        }
        let dummyAddress = dummyAddresses[indexPath.row]
        cell.setup(data: dummyAddress)
        return cell
        // Configure the cell
    }

    
    
}
