//
//  RideCollectionViewController.swift
//  Ride
//
//  Created by Tebengan Developer on 20/01/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

private let reuseIdentifier = "RideItemCell"
private let xibName = "RideItemCell"
private let collectionView = UICollectionView()
class RideCollectionViewController: UIViewController{
    let rideCollection: UICollectionView = {
        let backgroundColor = UIColor(red: CGFloat(244)/255, green: CGFloat(244)/255, blue: CGFloat(244)/255, alpha: 1)
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = backgroundColor
        collection.isScrollEnabled = true
        return collection
    }()
    
    let rides = [
        Ride(id: 1, departureDate: "Sen  Sel  Rab  Kam  Jum  Sab", departureTime: "09:00", originAddress: "Pluit Village ", originAddressDetail: "(4KM Jemput)Kota Tangerang Selatan ", destinationAddress: "Central Park Mall", destinationAddressDetail: "(4KM Antar) Kota Jakarta Pusat", availableSeat: 2),
        Ride(id: 2, departureDate: "Sen  Sel", departureTime: "09:00", originAddress: "Pluit Village ", originAddressDetail: "(4KM Jemput)Kota Tangerang Selatan ", destinationAddress: "Central Park Mall", destinationAddressDetail: "(4KM Antar) Kota Jakarta Pusat", availableSeat: 2),
        Ride(id: 3, departureDate: "Jum  Sab", departureTime: "09:00", originAddress: "Pluit Village ", originAddressDetail: "(4KM Jemput)Kota Tangerang Selatan ", destinationAddress: "Central Park Mall", destinationAddressDetail: "(4KM Antar) Kota Jakarta Pusat", availableSeat: 2),
        Ride(id: 4, departureDate: "Sen ", departureTime: "09:00", originAddress: "Pluit Village ", originAddressDetail: "(4KM Jemput)Kota Tangerang Selatan ", destinationAddress: "Central Park Mall", destinationAddressDetail: "(4KM Antar) Kota Jakarta Pusat", availableSeat: 2),
        Ride(id: 5, departureDate: "Sen  Sel  Rab  Kam  Jum  Sab Min", departureTime: "09:00", originAddress: "Pluit Village ", originAddressDetail: "(4KM Jemput)Kota Tangerang Selatan ", destinationAddress: "Central Park Mall", destinationAddressDetail: "(4KM Antar) Kota Jakarta Pusat", availableSeat: 2),
        Ride(id: 6, departureDate: "Sab  Min", departureTime: "09:00", originAddress: "Pluit Village ", originAddressDetail: "(4KM Jemput)Kota Tangerang Selatan ", destinationAddress: "Central Park Mall", destinationAddressDetail: "(4KM Antar) Kota Jakarta Pusat", availableSeat: 2)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        view.addSubview(rideCollection)
        rideCollection.register(UINib(nibName: xibName, bundle: Bundle(for: RideCollectionViewCell.self)), forCellWithReuseIdentifier: reuseIdentifier)
        rideCollection.delegate = self
        rideCollection.dataSource = self
        setupCollection()
        // Do any additional setup after loading the view.
    }
    
    func setupCollection(){
        rideCollection.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        rideCollection.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        rideCollection.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        rideCollection.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        rideCollection.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
    }


}

extension RideCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return rides.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RideCollectionViewCell
    
        // Configure the cell
        let ride = rides[indexPath.row]
        cell.setup(data: ride)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     return CGSize(width: collectionView.frame.width, height: 213)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 3, bottom: 3, right: 3)
    }
}

