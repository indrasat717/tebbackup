//
//  RideViewController.swift
//  Ride
//
//  Created by Tebengan Developer on 10/01/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

protocol RideList {
    func setupUI()
}

class RideListViewController: UISimpleSlidingTabController, RideList {
    private let label = UILabel()
    private let mainColor = UIColor(red: 0/255, green: 156/255, blue: 247/255, alpha: 1)
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        // view
        view.addSubview(label)
        
        setNavigation()
        
        // slidingTab
        setItem()
        setHeader()
        
        //setStyle(style: .fixed) // default fixed
        build() // build
    }
    
    
    func setNavigation(){
        let screenSize: CGRect = UIScreen.main.bounds
        let startingYPos = UIApplication.shared.statusBarFrame.size.height

        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width:  screenSize.width, height: 150))
        view.addSubview(navigationBar)

    }
    
    func setItem(){
        addItem(item: RideCollectionViewController(), title: "Menunggu") // add first item
        addItem(item: RideCollectionViewController(), title: "Diterima") // add second item
        addItem(item: RideCollectionViewController(), title: "Selesai") // add third item
    }
    
    func setHeader(){
        setHeaderActiveColor(color: .white) // default blue
        setHeaderInActiveColor(color: .lightText) // default gray
        setHeaderBackgroundColor(color: mainColor)
        setCurrentPosition(position: 1) // default 0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
