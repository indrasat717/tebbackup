//
//  RideCollectionViewCell.swift
//  Ride
//
//  Created by Tebengan Developer on 20/01/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

protocol RideCell {
    func setup( data: Ride)
}

private let AVAILABLE_SEAT = "Kursi Tersedia"

class RideCollectionViewCell: UICollectionViewCell, RideCell {
    @IBOutlet weak var departureDateLabel: UILabel!
    @IBOutlet weak var originAddressLabel: UILabel!
    @IBOutlet weak var originAddressDetail: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var destinationAddressDetail: UILabel!
    @IBOutlet weak var availableSeatLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

    }
    
    func setup(data: Ride) {
        self.departureDateLabel.text = data.departureDate
        self.originAddressLabel.text = data.originAddress
        self.originAddressDetail.text = data.originAddressDetail
        self.destinationAddress.text = data.destinationAddress
        self.destinationAddressDetail.text = data.destinationAddressDetail
        self.availableSeatLabel.text = String(data.availableSeat) + " " + AVAILABLE_SEAT
    }
}
