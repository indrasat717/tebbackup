//
//  Ride.swift
//  Ride
//
//  Created by Tebengan Developer on 16/01/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation
public class Ride: NSObject {
    public let id: Int
    public let departureDate: String
    public let departureTime: String
    public let originAddress: String
    public let originAddressDetail: String
    public let destinationAddress: String
    public let destinationAddressDetail: String
    public let availableSeat : Int
    
    // MARK: - Object Lifecycle
     public init(id: Int,
                 departureDate: String,
                 departureTime: String,
                 originAddress: String,
                 originAddressDetail: String,
                 destinationAddress: String,
                 destinationAddressDetail: String,
                 availableSeat: Int
                 ) {
        self.id = id
        self.departureDate = departureDate
        self.departureTime = departureTime
        self.originAddress = originAddress
        self.originAddressDetail = originAddressDetail
        self.destinationAddress = destinationAddress
        self.destinationAddressDetail = destinationAddressDetail
        self.availableSeat = availableSeat
     }
}
