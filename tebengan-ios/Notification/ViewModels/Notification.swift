//
//  Notification.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 30/09/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import Foundation

public class Notification : NSObject{
    public let id: Int
    public let title: String
    public let desc: String
    public let location: String
    public let date: String
    
    init(
        id: Int,
        title: String,
        desc: String,
        location: String,
        date: String
    ) {
        self.id = id
        self.title = title
        self.desc = desc
        self.location = location
        self.date = date
    }
}
// MARK: - Object Lifecycle

