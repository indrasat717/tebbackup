//
//  NotificationsViewController.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 21/01/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

private let reuseIdentifier = "NotificationCell"
private let cellIdentifier = "NotificationCell"
private let xibName = "NotificationCell"
class NotificationsViewController: UIViewController {
    
    let notificationCollection: UICollectionView = {
        let backgroundColor = UIColor(red: CGFloat(244)/255, green: CGFloat(244)/255, blue: CGFloat(244)/255, alpha: 1)
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = backgroundColor
        collection.isScrollEnabled = true
        return collection
    }()

    let notifications = [
        Notification(id: 1, title: "Perjalanan Akan Datang", desc: "Perjalanan Anda akan dimulai 2 jam lagi", location: "Purwormatani, Sleman - Pancoran, Jakarta Timur", date: "Kamis, 27 Feb 2017 - 15:00")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Register cell classes
        view.addSubview(notificationCollection)
        notificationCollection.register(UINib(nibName: xibName, bundle: Bundle(for: NotificationCollectionViewCell.self)), forCellWithReuseIdentifier: reuseIdentifier)
        notificationCollection.delegate = self
        notificationCollection.dataSource = self
        setupNavigation()
        setupCollection()
    }
    
    func setupNavigation(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = Color.blue
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func setupCollection(){

        notificationCollection.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        notificationCollection.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        notificationCollection.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 60).isActive = true
        notificationCollection.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        notificationCollection.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
    }

}

extension NotificationsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! NotificationCollectionViewCell
        
        // Configure the cell
        let notification = notifications[indexPath.row]
        cell.setup(data: notification)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 163)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
    }
    
}


