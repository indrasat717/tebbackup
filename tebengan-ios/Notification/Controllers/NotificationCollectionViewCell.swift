//
//  NotificationTableViewCell.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 30/09/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import UIKit

protocol NotificationCell {
    func setup( data: Notification)
}

class NotificationCollectionViewCell: UICollectionViewCell, NotificationCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setup(data: Notification){
        self.titleLabel.text = data.title
        self.descriptionLabel.text = data.desc
        self.locationLabel.text = data.location
        self.dateLabel.text = data.date
    }

}
