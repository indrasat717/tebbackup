//
//  AddressShortcutCollectionViewController.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 04/12/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import UIKit

private let reuseIdentifier = "AddressShortcutCell"
private let xibName = "AddressShortcutCollectionViewCell"
private let SHORTCUT_ADDRESS_TYPE_HOME = "home"
private let SHORTCUT_ADDRESS_TYPE_OFFICE = "office"

class AddressShortcutCollectionViewController: UICollectionViewController {
    let addressShortcuts = [AddressShortcut(id: 1, type: SHORTCUT_ADDRESS_TYPE_HOME, address: "Jl. Raya Serpong", image: "ImageHome", count: 12, subTitle: "Keluarga Menunggu anda di Rumah", description: "Perjalanan ke rumah bisa bareng pekerja yang juga punya keluarga sendiri."), AddressShortcut(id: 2, type: SHORTCUT_ADDRESS_TYPE_OFFICE, address: "Jl. Raya Serpong", image: "ImageOffice", count: 8, subTitle: "Perjalanan ke kantor lebih seru", description: "Perjalanan ke rumah bisa bareng pekerja yang juga punya keluarga sendiri. "), AddressShortcut(id: 3, type: SHORTCUT_ADDRESS_TYPE_HOME, address: "Jl. Raya Serpong", image: "ImageHomeTown", count: 20, subTitle: "Keluarga Menunggu anda di Kampung Halaman", description: "Perjalanan ke rumah bisa bareng pekerja yang juga punya keluarga sendiri.")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView.register(UINib(nibName: xibName, bundle: Bundle(for: AddressShortcutCollectionViewCell.self)), forCellWithReuseIdentifier: reuseIdentifier)

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    
    func numberOfSectionsInCollectionView(collectionView:
        UICollectionView!) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return addressShortcuts.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! AddressShortcutCollectionViewCell
        // Configure the cell
        let addressShortcut = addressShortcuts[indexPath.row]
        cell.setup(data: addressShortcut)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    

}
