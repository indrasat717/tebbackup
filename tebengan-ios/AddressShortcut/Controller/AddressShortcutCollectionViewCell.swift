//
//  AddressShortcutCollectionViewCell.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 03/12/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import UIKit

private let TEBENGAN_LABEL = "Tebengan"

protocol AddressShortcutCell {
    func setup( data: AddressShortcut)
}

class AddressShortcutCollectionViewCell: UICollectionViewCell, AddressShortcutCell {
    
    @IBOutlet weak var imageMarkIcon: UIImageView!

    @IBOutlet weak var imageAddressShortcut: UIImageView!
    
    @IBOutlet weak var labelRideCount: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.labelTitle.text = TEBENGAN_LABEL
    }
    
    public func setup( data: AddressShortcut){
        self.labelAddress.text = data.address
        self.labelRideCount.text = String(data.count)
        self.labelSubTitle.text = data.subTitle
        self.labelDescription.text = data.description
        self.imageAddressShortcut.image = UIImage(named: data.image)
    }

}
