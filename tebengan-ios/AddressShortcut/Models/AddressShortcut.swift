//
//  AddressShortcut.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 03/12/19.
//  Copyright © 2019 Tebengan Developer. All rights reserved.
//

import Foundation
struct AddressShortcut{
    var id: Int
    var type: String
    var address: String
    var image: String
    var count: Int
    var subTitle: String
    var description: String
}
