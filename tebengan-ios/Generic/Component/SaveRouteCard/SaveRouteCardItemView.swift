//
//  SaveRouteCardItemView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 22/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class SaveRouteCardItemView: XIBView {
    @IBOutlet private weak var cardView: UIView!
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var fromAddressLabel: UILabel!
    @IBOutlet private weak var toAddressLabel: UILabel!
    @IBOutlet private weak var footerView: UIView!
    @IBOutlet private weak var dashedView: UIView!
    
    override func initView() {
        headerView.isHidden = false
        headerView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        footerView.isHidden = false
        footerView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        dashedView.backgroundColor = .clear
        dashedView.createDashedLine(orientation: .vertical, color: UIColor.white.cgColor)
        cardView.layer.contents = UIImage(named: "SaveRouteCard")!.cgImage
    }
    
    func setData(from: String, to: String) {
        fromAddressLabel.text = from
        toAddressLabel.text = to
    }
}
