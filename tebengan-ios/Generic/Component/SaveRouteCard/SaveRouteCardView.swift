//
//  SaveRouteCardView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 22/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class SaveRouteCardView: XIBView {
    
    @IBOutlet private weak var mainStackView: UIStackView!
    
    override func initView() {
        setMainStackView()
    }
    
    struct DummyRoute {
        let from: String
        let to: String
    }
    
    let dummyList = [DummyRoute(from: "Tebengan Indonesia", to: "Mall Teras Kota Tangerang"),
                     DummyRoute(from: "Supermall Karawaci", to: "Alam Sutera"),
                     DummyRoute(from: "Gb. Tol Kebun Jeruk 2 Jakbar", to: "Bumi Serpong Damai"),
                     DummyRoute(from: "Perumnas 2 Tangerang", to: "Bogor"),
                     DummyRoute(from: "Stasiun Rw Buntu", to: "Stasiun Rawa Buaya"),]
}

private extension SaveRouteCardView {
    func setMainStackView() {
        for (_, data) in dummyList.enumerated() {
            let itemView = SaveRouteCardItemView()
            itemView.setData(from: data.from, to: data.to)
            mainStackView.addArrangedSubview(itemView)
        }
    }
}
