//
//  ChatItemView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 23/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class ChatItemView: XIBView {

    @IBOutlet private weak var badgeView: UIView!
    @IBOutlet private weak var userImage: UIImageView!
    
    override func initView() {
        userImage.layer.cornerRadius = userImage.frame.height/2
        badgeView.layer.cornerRadius = badgeView.frame.height/2
    }

}
