//
//  Gender.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 14/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

protocol GenderViewProtocol {
    func setup()
    func setBorderWidth(_ button: UIButton, _ width: CGFloat)
    func setCornerRadius(_ button: UIButton , _ width: CGFloat)
    func setBackgroundColor(_ button: UIButton, _ color: UIColor)
    func setBorderColor(_ button: UIButton, _ color: CGColor)
    func activeButton(_ button: UIButton)
    func inActiveButton(_ button: UIButton)
}

class GenderView: XIBView, GenderViewProtocol {
    @IBOutlet private var maleButton: UIButton!
    @IBOutlet private var femaleButton: UIButton!
    
    private let cornerRadius = CGFloat(4)
    private let borderWidth = CGFloat(1)
    private let noBorder = CGFloat(0)
    
    private let maleValue = 7
    private let femaleValue = 8
    
    override func initView() {
        setup()
    }
    
    
    var selectedIndex: Int {
        get {
            return 0
        } set {
            if newValue == maleValue {
                activateMaleButton()
            }else if newValue == femaleValue {
                activateFemaleButton()
            }
        }
    }
    
    internal func setup(){
        selectedIndex = 7
        setBorderWidth(maleButton, noBorder)
        setBorderWidth(femaleButton, noBorder)
        setCornerRadius(maleButton, cornerRadius)
        setCornerRadius(femaleButton, cornerRadius)
    }
    
    
    @IBAction private func onMaleIsTapped(_ sender: Any) {
        self.selectedIndex = maleValue
        activateMaleButton()
    }
    
    @IBAction private func onFemaleIsTapped(_ sender: Any) {
        self.selectedIndex = femaleValue
        activateFemaleButton()
    }
    
    private func activateMaleButton(){
        activeButton(maleButton)
        inActiveButton(femaleButton)
    }
    
    private func activateFemaleButton(){
        activeButton(femaleButton)
        inActiveButton(maleButton)
    }
    
    internal func setBorderWidth(_ button: UIButton, _ width: CGFloat){
        button.layer.borderWidth = width
    }
    
    internal func setCornerRadius(_ button: UIButton , _ width: CGFloat){
        button.layer.cornerRadius = width
    }
    
    internal func setBackgroundColor(_ button: UIButton, _ color: UIColor){
        button.backgroundColor = color
    }
    
    internal func setBorderColor(_ button: UIButton, _ color: CGColor){
        button.layer.borderColor = color
    }
    
    internal func activeButton(_ button: UIButton){
        setBorderWidth(button, self.borderWidth)
        setBackgroundColor(button, Color.buttonOptionSelected)
        setBorderColor(button, Color.buttonOptionBorderSelected.cgColor)
    }
    
    internal func inActiveButton(_ button: UIButton){
        setBorderWidth(button, noBorder)
        setBackgroundColor(button, Color.buttonOption)
        setBorderColor(button, UIColor.clear.cgColor)
    }

}


