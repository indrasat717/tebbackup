//
//  OneLineLabelView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 27/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class OneLineLabelView: XIBView {

    @IBOutlet private weak var holderView: UIView!
    @IBOutlet private weak var oneLineLabel: BaseLabel!

    func setData(title: String) {
        oneLineLabel.text = title
        oneLineLabel.setFontRegular(size: 12)
        holderView.backgroundColor = .clear
    }
}
