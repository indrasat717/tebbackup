//
//  RightIconTwoLabelView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 28/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class RightIconTwoLabelView: XIBView {
    
    @IBOutlet private weak var topLabel: BaseLabel!
    @IBOutlet private weak var bottomLabel: BaseLabel!
    @IBOutlet private weak var leftIconImageVIew: UIImageView!
    @IBOutlet private weak var rightIconImageVIew: UIImageView!
    @IBOutlet private weak var leftIconWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var leftIconHeightConstraint: NSLayoutConstraint!
    
    func setTwoLabel(topLabels: String, bottomLabels: String, leftIcons: String, rightIcons: String) {
        topLabel.text = topLabels
        topLabel.setFontBold(size: 14)
        bottomLabel.text = bottomLabels
        bottomLabel.setFontRegular(size: 12)
        leftIconWidthConstraint.constant = 40
        leftIconHeightConstraint.constant = 40
        //leftIcon.image = UIImage(named: leftIcons)
        //leftIcon.layer.cornerRadius = leftIconHeightConstraint.constant/2
        //rightIcon.image = UIImage(named: rightIcons)
    }
    
    func setOneLabel(topLabel: String, leftIcon: String, rightIcon: String) {
        self.topLabel.text = topLabel
        self.bottomLabel.isHidden = true
        //self.leftIcon.image = UIImage(named: leftIcon)
        //self.rightIcon.image = UIImage(named: rightIcon)
    }
    
    func setData(_ topValue: String?, _ bottomValue: String?, _ leftIcon: String?, _ rightIcon: String?) {
        topLabel.text = topValue
        topLabel.isHidden = topValue == nil
        bottomLabel.text = bottomValue
        bottomLabel.isHidden = bottomValue == nil
        leftIconImageVIew.image = UIImage(named: leftIcon ?? "AppIcon")
        rightIconImageVIew.image = UIImage(named: rightIcon ?? "AppIcon")
    }
    
    func setViewPersonalSection(_ isUseCustomImage: Bool) {
        if isUseCustomImage {
            leftIconWidthConstraint.constant = 40
            leftIconHeightConstraint.constant = 40
            leftIconImageVIew.layer.cornerRadius = leftIconHeightConstraint.constant/2
        }
        topLabel.textColor = Color.black
        topLabel.setFontRegular(size: 14)
        bottomLabel.textColor = Color.gray
        bottomLabel.setFontRegular(size: 12)
    }
}
