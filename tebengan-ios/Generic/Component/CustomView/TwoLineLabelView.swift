//
//  TwoLineLabelView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 9/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class TwoLineLabelView: XIBView {
    
    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var bottomLabel: UILabel!
    @IBOutlet private weak var holderView: UIView!
    
    override func initView() {
        super.initView()
        backgroundColor = .clear
        holderView.backgroundColor = .clear
    }
        
    func setData(topValue: String, bottomValue: String) {
        topLabel.text = topValue
        topLabel.font = UIFont(name: "AvenirNext-Medium", size: 16)
        bottomLabel.text = bottomValue
        bottomLabel.font = UIFont(name: "AvenirNext-Regular", size: 14)
    }
}
