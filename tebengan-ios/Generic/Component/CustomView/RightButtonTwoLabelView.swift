//
//  RightButtonTwoLabelView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 28/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
import SDWebImage

class RightButtonTwoLabelView: XIBView {
    @IBOutlet private weak var topLabel: BaseLabel!
    @IBOutlet private weak var bottomLabel: BaseLabel!
    @IBOutlet weak var leftIconImageView: UIImageView!
    @IBOutlet weak var rightButton: UIButton!
    
    @IBOutlet private weak var leftIconWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var leftIconHeightConstraint: NSLayoutConstraint!
    
    override func initView() {
        super.initView()
        rightButton.layer.borderWidth = 1
        rightButton.layer.cornerRadius = 5
        rightButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func setData(_ topValue: String?, _ bottomValue: String?, _ leftIcon: String?) {
        topLabel.text = topValue
        topLabel.isHidden = topValue == nil
        bottomLabel.text = bottomValue
        bottomLabel.isHidden = bottomValue == nil
        leftIconImageView.image = UIImage(named: leftIcon ?? "AppIcon")
    }
    
    func setLeftIcon(withURL url: String) {
        let imageURL = URL(string: url)
        leftIconImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        leftIconImageView.sd_setImage(with: imageURL, completed: nil)
    }
    
    func setAddressSection(isAlreadySet: Bool) {
        if isAlreadySet {
            topLabel.textColor = Color.lightBlue
            bottomLabel.textColor = Color.gray
            rightButton.setTitle("Hapus", for: .normal)
            rightButton.setTitleColor(Color.lightRed, for: .normal)
            rightButton.layer.borderColor = Color.lightRed.cgColor
        } else {
            topLabel.textColor = Color.black
            bottomLabel.textColor = Color.lightOrange
            rightButton.setTitle("Tambah", for: .normal)
            rightButton.setTitleColor(Color.lightBlue, for: .normal)
            rightButton.layer.borderColor = Color.lightBlue.cgColor
        }
        topLabel.setFontRegular(size: 14)
        bottomLabel.setFontRegular(size: 12)
    }
    
    func setListVehicleView() {
        leftIconWidthConstraint.constant = 40
        leftIconHeightConstraint.constant = 40
        leftIconImageView.layer.cornerRadius = 2
        leftIconImageView.contentMode = .scaleAspectFill
        
        rightButton.setTitle("Hapus", for: .normal)
        rightButton.setTitleColor(Color.lightRed, for: .normal)
        rightButton.layer.borderColor = Color.lightRed.cgColor
        
        topLabel.textColor = Color.black
        topLabel.setFontRegular(size: 14)
        bottomLabel.textColor = .black
        bottomLabel.setFontRegular(size: 12)
    }
}
