//
//  AddressShortcutItemView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 28/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class AddressShortcutItemView: XIBView {
    
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    
    func setData(address: String, type: AddressShortcutView.AddressType) {
        addressLabel.text = address
        switch type {
        case .home:
            imageView.image = UIImage(named: "ImageHome")
        case .office:
            imageView.image = UIImage(named: "ImageOffice")
        case .hometown:
            imageView.image = UIImage(named: "ImageHomeTown")
        }
    }
    
}
