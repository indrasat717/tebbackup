//
//  AddressShortcutView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 28/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class AddressShortcutView: XIBView {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var mainStackView: UIStackView!
    
    override func initView() {
        setMainStackView()
    }
    
    struct Address {
        let address: String
        let type: AddressType
    }
    
    enum AddressType {
        case home, office, hometown
    }
    
    let dummyList = [Address(address: "Karawaci CyberPark", type: .home),
                     Address(address: "Tebengan Id", type: .office),
                     Address(address: "Sukabumi", type: .hometown)]
    
}

private extension AddressShortcutView {
    func setMainStackView() {
        for (_, data) in dummyList.enumerated() {
            let itemView = AddressShortcutItemView()
            itemView.setData(address: data.address, type: data.type)
            mainStackView.addArrangedSubview(itemView)
        }
    }
}
