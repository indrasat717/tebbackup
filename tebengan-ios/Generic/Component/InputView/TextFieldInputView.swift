//
//  TextFieldInputView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class TextFieldInputView: XIBView {
    @IBOutlet private weak var placeholderLabel: UILabel!
    @IBOutlet private weak var leftIconImageView: UIImageView!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var rightIconImageView: UIImageView!
    @IBOutlet private weak var borderView: UIView!
    @IBOutlet private weak var borderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var borderViewBottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet private weak var placeholderYConstraint: NSLayoutConstraint!
    @IBOutlet private weak var placeholderLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var messageBottomLabel: UILabel!
    
    public var textObs: ControlProperty<String?> { get { return self.textField.rx.text } }
    
    @IBInspectable var text: String {
        get {
            return textField.text ?? ""
        } set {
            textField.text = newValue
        }
    }
    
    @IBInspectable var placeholder: String {
        get {
            return placeholderLabel.text ?? ""
        } set {
            placeholderLabel.text = newValue
        }
    }
    
    @IBInspectable var leftIcon: UIImage {
        get {
            return leftIconImageView.image ?? UIImage()
        } set {
            leftIconImageView.image = newValue
        }
    }
    
    @IBInspectable var rightIcon: UIImage {
        get {
            return rightIconImageView.image ?? UIImage()
        } set {
            rightIconImageView.image = newValue
        }
    }
    
    @IBInspectable var msgError: String {
        get {
            return messageBottomLabel.text ?? ""
        } set {
            messageBottomLabel.text = newValue
            messageBottomLabel.textColor = .red
            messageBottomLabel.isHidden = false
            borderViewBottomSpaceConstraint.constant = 20
        }
    }

    @IBInspectable var inputType: String {
        get {
            return textFieldType.rawValue
        } set {
            if let inputType = TextFieldType(rawValue: newValue.lowercased()) {
                textFieldType = inputType
                setTextField()
            }
        }
    }
    
    @IBInspectable var maxLength: Int = 0
    
    private enum TextFieldType: String {
        case email = "email"
        case string = "string"
        case password = "password"
    }
    
    private var textFieldType: TextFieldType = .string
    
    override func initView() {
        textField.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        leftIconImageView.isHidden = leftIconImageView.image == nil
        rightIconImageView.isHidden = rightIconImageView.image == nil
    }
}

private extension TextFieldInputView {
    func changeBorderViewState(isFocus: Bool) {
        if isFocus {
            borderView.backgroundColor = .blue
            borderViewHeightConstraint.constant = 2
        } else {
            borderView.backgroundColor = .lightGray
            borderViewHeightConstraint.constant = 1
        }
    }
    
    func movePlaceholderPosition(isToTop: Bool) {
        if isToTop {
            placeholderYConstraint.constant = leftIconImageView.isHidden ? -20 : -23
            placeholderLeadingConstraint.constant = leftIconImageView.isHidden ? 0 : -25
        } else {
            placeholderYConstraint.constant = 0
            placeholderLeadingConstraint.constant = 0
        }
    }
    
    func setTextField() {
        switch textFieldType {
        case .email:
            textField.keyboardType = .emailAddress
        case .password:
            toggleSecureTextEntry()
            rightIconImageView.tap(UITapGestureRecognizer(target: self, action: #selector(toggleSecureTextEntry)))
        default:
            break
        }
    }
    
    @objc func toggleSecureTextEntry() {
        textField.isSecureTextEntry.toggle()
        rightIconImageView.image = textField.isSecureTextEntry ? #imageLiteral(resourceName: "eyeOn") : #imageLiteral(resourceName: "eyeOff")
    }
}

extension TextFieldInputView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        changeBorderViewState(isFocus: true)
        movePlaceholderPosition(isToTop: true)
        animate(animations: layoutIfNeeded, completion: nil)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        changeBorderViewState(isFocus: false)
        if textField.text == "" {
            movePlaceholderPosition(isToTop: false)
        }
        animate(animations: layoutIfNeeded, completion: nil)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newMaxLength = maxLength == 0 ? 100 : maxLength
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= newMaxLength
    }
}
