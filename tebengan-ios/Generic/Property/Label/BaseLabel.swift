//
//  BaseLabel.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 10/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {
    
    func setFontRegular(size: CGFloat) {
        font = UIFont(name: "AvenirNext-Regular", size: size)
    }
    
    func setFontMedium(size: CGFloat) {
        font = UIFont(name: "AvenirNext-Medium", size: size)
    }
    
    func setFontBold(size: CGFloat) {
        font = UIFont(name: "AvenirNext-Bold", size: size)
    }
    
    func setFontBoldItalic(size: CGFloat) {
        font = UIFont(name: "AvenirNext-BoldItalic", size: size)
    }
    
    func setFontDemiBold(size: CGFloat) {
        font = UIFont(name: "AvenirNext-DemiBold", size: size)
    }
    
    func setFontDemiBoldItalic(size: CGFloat) {
        font = UIFont(name: "AvenirNext-DemiBoldItalic", size: size)
    }
    
    func setFontItalic(size: CGFloat) {
        font = UIFont(name: "AvenirNext-Italic", size: size)
    }

    func setFontMediumItalic(size: CGFloat) {
        font = UIFont(name: "AvenirNext-MediumItalic", size: size)
    }
    
    func setFontUltraLight(size: CGFloat) {
        font = UIFont(name: "AvenirNext-UltraLight", size: size)
    }

    func setFontUltraLightItalic(size: CGFloat) {
        font = UIFont(name: "AvenirNext-UltraLightItalic", size: size)
    }
    
}
