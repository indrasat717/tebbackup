//
//  TableViewDelegate.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 9/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

protocol TableViewDelegate: class {
    func triggerEvent(_ tableView: BaseTableView, type: TableViewDelegateType)
}

enum TableViewDelegateType {
    case tappable(_ model: Decodable)
}
