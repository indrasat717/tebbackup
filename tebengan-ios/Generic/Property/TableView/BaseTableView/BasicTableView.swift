//
//  BasicTableView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class BasicTableView: BaseTableView {
    
    weak var eventDelegate: TableViewDelegate?
    
    override func initView() {
        delegate = self
        dataSource = self
    }
    
    func getBasicSectionCount() -> Int {
        return 1
    }
    
    func getBasicRowCount(section: Int) -> Int? {
        debugPrint("required to override getBasicRowCount")
        return 0
    }
    
    func getBasicCell(indexPath: IndexPath) -> UITableViewCell {
        debugPrint("required to override getBasicCell")
        return UITableViewCell()
    }
    
    func getBasicRowHeight(indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getBasicHeaderView(section: Int) -> UIView? {
        return nil
    }
    
    func getBasicHeaderHeight(section: Int) -> CGFloat {
        return 0
    }
    
    func getBasicFooterView(section: Int) -> UIView? {
        return nil
    }
    
    func getBasicFooterHeight(section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func handleDidSelectRowAt(indexPath: IndexPath) {
        // override to handle this func
    }
}

extension BasicTableView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return getBasicSectionCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getBasicRowCount(section: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getBasicCell(indexPath: indexPath)
    }
}

extension BasicTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getBasicRowHeight(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getBasicHeaderView(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getBasicHeaderHeight(section: section)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return getBasicFooterView(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getBasicFooterHeight(section: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        handleDidSelectRowAt(indexPath: indexPath)
    }
}
