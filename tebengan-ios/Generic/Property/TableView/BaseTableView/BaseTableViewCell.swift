//
//  BaseTableViewCell.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    // MARK: - Initializer
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    func getCustomView() -> UIView {
        return UIView()
    }
    
    func initView() {
        initCustomView()
    }
    
    func getTopConstraint() -> CGFloat? {
        return 0
    }
    
    func getLeadingConstraint() -> CGFloat? {
        return 0
    }
    
    func getBottomConstraint() -> CGFloat? {
        return 0
    }
    
    func getTrailingConstraint() -> CGFloat? {
        return 0
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

private extension BaseTableViewCell {
    func initCustomView() {
        let customView = getCustomView()
        addSubviewWithConstraint(item: customView, top: getTopConstraint(), leading: getLeadingConstraint(), bottom: getBottomConstraint(), trailing: getTrailingConstraint())
        selectionStyle = .none
    }
}
