//
//  BaseTableView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 24/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class BaseTableView: UITableView {
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    @objc dynamic func initView() {
        // ovveride this function to use lifecycle like viewDidLoad in ViewController
    }
    
}

