//
//  TwoLineLabelViewCell.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 9/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class TwoLineLabelViewCell: BaseTableViewCell {
    override func getCustomView() -> UIView {
        let view = TwoLineLabelView()
        view.tag = 1
        return view
    }
}
