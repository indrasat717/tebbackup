//
//  OTPTextField.swift
//  OTPField
//  tebengan-ios
//
//  Created by Tebengan Developer on 26/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation
import UIKit

class OTPTextField: UITextField {
    
    var previousTextField: OTPTextField?
    var nextTextField: OTPTextField?

    override public func deleteBackward(){
        if text == "" {
            previousTextField?.becomeFirstResponder()
        }
    }
    
}
