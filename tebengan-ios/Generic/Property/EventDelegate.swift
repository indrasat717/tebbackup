//
//  EventDelegate.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 10/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

protocol EventDelegate: class {
    func triggerEvent(_ type: EventDelegateType)
}

extension EventDelegate {
    func triggerEvent(_ type: EventDelegateType) {}
}

enum EventDelegateType {
    case choose(_ model: Decodable)
}
