//
//  AppStoryboard.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 10/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

enum AppStoryboard: String {
    /// Add new storyboard name here
    case EditProfile
    case SearchAddress
    case Vehicle
    
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
    
    func viewController<T: UIViewController >(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    class var storyboardID: String {
        return "\(self)"
    }
    
    static func instantiate(appStoryboard storyboard: AppStoryboard) -> Self {
        return storyboard.viewController(viewControllerClass: self)
    }
}
