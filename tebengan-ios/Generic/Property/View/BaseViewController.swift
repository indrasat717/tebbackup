//
//  BaseViewController.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 26/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

typealias AlertFunction = (UIAlertAction) -> Void

class BaseViewController: UIViewController {
    
    private var loadingOverlayView: LoadingOverlayView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "AvenirNext-DemiBold", size: 16)!]
    }
    
    func showLoading(_ isLoading: Bool){
        if isLoading {
            addOverlayLoadingView()
        }else{
            removeOverlayLoadingView()
        }
    }
    
    func addOverlayLoadingView() {
        if loadingOverlayView == nil {
            loadingOverlayView = LoadingOverlayView(frame: UIScreen.main.bounds)
            UIApplication.shared.keyWindow?.addSubview(loadingOverlayView!)
        }
    }
    
    func removeOverlayLoadingView() {
        loadingOverlayView?.removeFromSuperview()
        loadingOverlayView = nil
    }
    
    func presentAlertOKHandler(title: String? = nil, message: String?, handler: AlertFunction? = nil) {
        presentAlert(title: title, message: message, actions: [UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: handler)])
    }
    
    func presentAlert(title: String? = nil, message: String? = nil, actions: [UIAlertAction], preferredAction: UIAlertAction? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alert.addAction(action)
        }
        alert.preferredAction = preferredAction
        present(alert, animated: true, completion: nil)
    }
}
