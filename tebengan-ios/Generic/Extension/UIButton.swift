//
//  UIButton.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 05/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func enableButton(_ isEnable: Bool){
        if isEnable{
            self.backgroundColor = Color.primaryButton
        }else{
            self.backgroundColor = Color.disableButton
        }
    }
}
