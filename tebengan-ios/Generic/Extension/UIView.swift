//
//  UIView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 22/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

extension UIView {
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func createDashedLine(orientation: Orientation, color: CGColor) {
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = color
        lineLayer.lineWidth = 1
        lineLayer.lineDashPattern = [2,3]
        let cgPath = CGMutablePath()
        var cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: 0, y: frame.size.height)]
        if orientation == .horizontal {
            cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: frame.size.width, y: 0)]
        }
        cgPath.addLines(between: cgPoint)
        lineLayer.path = cgPath
        layer.addSublayer(lineLayer)
    }
    
    func addSubviewWithConstraint(item: UIView, top: CGFloat? = nil, leading: CGFloat? = nil, bottom: CGFloat? = nil, trailing: CGFloat? = nil) {
        item.translatesAutoresizingMaskIntoConstraints = false
        addSubview(item)
        if let top = top {
            item.addTopConstraint(to: self, constant: top)
        }
        if let bottom = bottom {
            item.addBottomConstraint(to: self, constant: bottom * -1)
        }
        if let leading = leading {
            item.addLeadingConstraint(to: self, constant: leading)
        }
        if let trailing = trailing {
            item.addTrailingConstraint(to: self, constant: trailing * -1)
        }
    }
    
    func addSubviewCenterConstraint(item: UIView) {
        item.translatesAutoresizingMaskIntoConstraints = false
        addSubview(item)

        addCenterXConstraint(to: item)
        addCenterYConstraint(to: item)
    }
    
    func addCenterXConstraint(to item: UIView) {
        centerXAnchor.constraint(equalTo: item.centerXAnchor).isActive = true
    }

    func addCenterYConstraint(to item: UIView) {
        centerYAnchor.constraint(equalTo: item.centerYAnchor).isActive = true
    }
    
    func addTopConstraint(to item: UIView, constant: CGFloat) {
        topAnchor.constraint(equalTo: item.topAnchor, constant: constant).isActive = true
    }
    
    func addBottomConstraint(to item: UIView, constant: CGFloat, priority: Float = 1000) {
        let temp = bottomAnchor.constraint(equalTo: item.bottomAnchor, constant: constant)
        temp.priority = UILayoutPriority(rawValue: priority)
        temp.isActive = true
    }
    
    func addLeadingConstraint(to item: UIView, constant: CGFloat, priority: Float = 1000) {
        let temp = leadingAnchor.constraint(equalTo: item.leadingAnchor, constant: constant)
        temp.priority = UILayoutPriority(rawValue: priority)
        temp.isActive = true
    }
    
    func addTrailingConstraint(to item: UIView, constant: CGFloat) {
        trailingAnchor.constraint(equalTo: item.trailingAnchor, constant: constant).isActive = true
    }
    
    func tap(_ tapGestureRecognizer: UITapGestureRecognizer) -> Void {
        isUserInteractionEnabled = true
        gestureRecognizers?.removeAll()
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    func createCardView(){
        // The subview inside the collection view cell
        layer.shadowRadius = 4.0
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width:5, height: 5)
    }
    
    func animate(animations: @escaping () -> Void, completion: ((Bool) -> Void)?) {
        UIView.animate(
            withDuration: 0.2,
            delay: 0.0,
            options: .curveLinear,
            animations: animations,
            completion: completion
        )
    }
}
