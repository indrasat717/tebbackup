//
//  String.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 27/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

extension Int{
    
    func intToMinutesStringFormat() -> String{
        let time = self
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        let text = String(format:"%d:%02d", minutes, seconds)
        return text
    }
    
}
