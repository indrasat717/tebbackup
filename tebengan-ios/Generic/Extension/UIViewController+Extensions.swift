//
//  UIViewController+Extensions.swift
//  MVVM-RxSwift-LoginFlow
//
//  Created by Tebengan Developer on 19/02/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentError(_ error: Error) {
        let alertController = UIAlertController(title: "Error",
                                                message: error.localizedDescription,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: "OK", style: .default))
        self.present(alertController, animated: true)
    }
    func presentMessage(_ message: String) {
        let alertController = UIAlertController(title: "Message",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: "OK", style: .default))
        self.present(alertController, animated: true)
    }
}
