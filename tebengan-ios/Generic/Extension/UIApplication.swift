//
//  UIApplication.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 23/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            if #available(iOS 13.0, *) {
                return statusBarUIView?.backgroundColor
            }
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            if #available(iOS 13.0, *) {
                statusBarUIView?.backgroundColor = newValue
            } else {
                (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
            }
            
        }
    }

    class var statusBarUIView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 021 // random int to give identity of statusBarTag

            if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
                return statusBar
            }
            else {
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = tag

                UIApplication.shared.keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }

}
