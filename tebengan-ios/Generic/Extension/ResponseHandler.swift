//
//  ResponseHandler.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 26/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

class ResponseHandler {
    static func getResponse<T : Decodable> (data: Data) -> T? {
        do {
            let response = try JSONDecoder().decode(T.self, from: data)
            return response
        } catch let error {
            debugPrint(error)
        }
        return nil
    }
}
