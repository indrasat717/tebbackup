//
//  String.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 23/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

extension String {
    func getJSONSerialize() -> Any? {
        if let data = self.data(using: .utf8) {
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                return response
            } catch let error {
                debugPrint(error)
            }
        }
        return nil
    }
}
