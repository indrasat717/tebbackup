//
//  UIStackView.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 3/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

extension UIStackView {
    func addBackgroundView(color: UIColor, frame: CGRect? = nil) {
        let subView = UIView(frame: frame ?? bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
