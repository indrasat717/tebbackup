//
//  Reactive.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 02/03/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

extension Reactive where Base: TextFieldInputView {
    var text: Binder<String?> {
      // `base` is a property of `Reactive` and of type `TexFieldInputVIew`
        return Binder(self.base) { (view, newValue) in
            view.text = newValue ?? ""
      }
  }
}
