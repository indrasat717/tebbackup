//
//  Color.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 23/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

struct Color {
    static let blue = UIColor.init(hexInt: 0x169EF4)
    static let red = UIColor.init(hexString: "#fc031c")
    static let gray = UIColor.init(red: 133, green: 133, blue: 133)
    static let black = UIColor.init(red: 64, green: 72, blue: 82)
    static let primaryButton = UIColor.green
    static let disableButton = UIColor.init(red: 169, green: 188, blue: 200)
    static let buttonOption = UIColor.init(red: 240, green: 240, blue: 240)
    static let buttonOptionSelected = UIColor.init(red: 118, green: 196, blue: 255, alpha: 0.09)
    static let buttonOptionBorderSelected = UIColor.init(red: 103, green: 155, blue: 255)
    
    static let lightRed = UIColor.init(red: 255, green: 82, blue: 82)
    static let lightBlue = UIColor.init(red: 0, green: 156, blue: 247)
    static let lightOrange = UIColor.init(red: 255, green: 152, blue: 0)
    static let lightGray = UIColor.init(red: 216, green: 216, blue: 216)

}
