//
//  DeviceConfig.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 23/1/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

enum Orientation {
    case vertical
    case horizontal
}

enum ScrollViewGestureSwipe {
    case none
    case downToTop
    case topToDown
}
