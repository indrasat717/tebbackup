//
//  LoginViewModel.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 25/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Moya
import RxSwift
import FBSDKLoginKit

class LoginViewModel {
    
    var showLoadingClosure: ((Bool) -> Void)?
    var showAlertClosure: ((_ title: String, _ msg: String) -> Void)?
    
    private let disposeBag = DisposeBag()
    private let provider = MoyaProvider<LoginService>()
    
    func requestLoginMail(email: String, password: String) {
        showLoadingClosure?(true)
        provider.rx.request(.loginMail(email, password))
            .filterSuccessfulStatusCodes()
            .subscribe { [weak self] (event) in
                self?.showLoadingClosure?(false)
                switch event {
                case .success(let response):
                    if let data: LoginModel = ResponseHandler.getResponse(data: response.data) {
                        self?.registFirebaseToken(tebenganToken: data.id, userId: data.userId)
                        UserDefaults.standard.set(data.id, forKey:"token")
                        UserDefaults.standard.set(data.userId, forKey:"userId")
                        UserDefaults.standard.synchronize()
                    }
                case .error(let error):
                    self?.handleError(error: error)
                }
        }.disposed(by: disposeBag)
    }
    
    func registFirebaseToken(tebenganToken: String, userId: Int) {
        showLoadingClosure?(true)
        /// TODO: Implement Firebase Token here
        let firebaseToken = "000000000000"
        provider.rx.request(.registFirebaseToken(tebenganToken, firebaseToken))
            .filterSuccessfulStatusCodes()
            .subscribe { [weak self] (event) in
                self?.showLoadingClosure?(false)
                switch event {
                case .success(let response):
                    print(response)
                case .error(let error):
                    self?.handleError(error: error)
                }
        }.disposed(by: disposeBag)
    }
    
    func getUserDetail(userId: Int) {
        showLoadingClosure?(true)
        provider.rx.request(.userDetail(userId: userId))
            .filterSuccessfulStatusCodes()
            .subscribe { [weak self] (event) in
                self?.showLoadingClosure?(false)
                switch event {
                case .success(let response):
                    if let userModel: UserModel = ResponseHandler.getResponse(data: response.data) {
                        self?.showAlertClosure?("Welcome", userModel.data.userById.name ?? "")
                    }
                case .error(let error):
                    self?.handleError(error: error)
                }
        }.disposed(by: disposeBag)
    }
    
    func requestLoginFacebook() {
        showLoadingClosure?(true)
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: ["public_profile", "email"], from: nil) { [weak self] (result, error) in
            self?.showLoadingClosure?(false)
            if let error = error {
                debugPrint(error.localizedDescription)
            } else if let result = result {
                if result.isCancelled {
                    debugPrint("Login facebook canceled")
                } else {
                    if result.declinedPermissions.count > 0 || result.grantedPermissions.count < 2 {
                        debugPrint("Login dengan facebook menbutuhkan akses profile dan email Anda")
                    } else {
                        guard let token = result.token else {
                            debugPrint("Token facebook is gone")
                            return
                        }
                        self?.getDetailFacebook(accessToken: token.tokenString)
                    }
                }
            }
        }
    }
    
    func getDetailFacebook(accessToken: String) {
        showLoadingClosure?(true)
        provider.rx.request(.userDetailFacebook(accessToken: accessToken))
            .filterSuccessfulStatusCodes()
            .subscribe { [weak self] (event) in
                self?.showLoadingClosure?(false)
                switch event {
                case .success(let response):
                    if let userModel: UserFacebookModel = ResponseHandler.getResponse(data: response.data) {
                        print(userModel)
                    }
                case .error(let error):
                    self?.handleError(error: error)
                }
        }.disposed(by: disposeBag)
    }
    
    func requestLoginLinkedin() {
        print("login with linkedin")
    }
}


private extension LoginViewModel {
    func handleError(error: Error) {
        if let moyaError = error as? MoyaError,
            let dataResponse = moyaError.response?.data,
            let errorModel: ErrorModel = ResponseHandler.getResponse(data: dataResponse) {
            showAlertClosure?(errorModel.error.name, errorModel.error.message)
            return
        }
        showAlertClosure?("Error", error.localizedDescription)
    }
}
