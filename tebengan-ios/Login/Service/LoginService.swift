//
//  LoginService.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 26/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Moya
import Foundation

enum LoginService {
    case userDetail(userId: Int)
    case userDetailFacebook(accessToken: String)
    case loginMail(_ email: String, _ password: String)
    case registFirebaseToken(_ tebenganToken: String, _ firebaseToken: String)
}

extension LoginService: TargetType {
    var baseURL: URL {
        switch self {
        case .userDetail(_):
            return App.graphQLHost
        case .userDetailFacebook(_):
            return App.graphFacebookHost
        default:
            return App.tebenganHost
        }
    }
    
    var path: String {
        switch self {
        case .loginMail(_, _):
            return "UserModels/login"
        case .registFirebaseToken(_, _):
            return "UserModels/_registerFirebaseToken"
        case .userDetail(_):
            return "graphql"
        case .userDetailFacebook(_):
            return "me"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .loginMail(_, _),
             .userDetail(_),
             .userDetailFacebook(_),
             .registFirebaseToken(_, _):
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .loginMail(let email, let password):
            var params: [String: Any] = [:]
            params["email"] = email
            params["password"] = password
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
            
        case .registFirebaseToken(_, let firebaseToken):
            var params: [String: Any] = [:]
            params["token"] = firebaseToken
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
            
        case .userDetail(let userId):
            let query = """
                            query {
                                userById(id: \(userId)) {
                                nodeId
                                genderId
                                name
                                email
                                familyName
                                avatarUrl
                                isEmailConfirmed
                                isPhoneVerified
                                phoneNumber
                                description
                                facebookId
                                linkedinId
                                }
                            }
                        """
            var params: [String: Any] = [:]
            params["query"] = query
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
            
        case .userDetailFacebook(_):
            var params: [String: Any] = [:]
            params["fields"] = "email, first_name, last_name, picture"
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        var header: [String: String] = [:]
        header[App.Header.contentType.rawValue] = App.HeaderValue.appsJson.rawValue
        switch self {
        case .registFirebaseToken(let tebenganToken, _):
            header[App.Header.authorization.rawValue] = tebenganToken
        case .userDetail(_):
            header[App.Header.client.rawValue] = App.HeaderValue.apps.rawValue
            header[App.Header.graphqlauth.rawValue] = App.HeaderValue.tebenganGraphAuth.rawValue
        case .userDetailFacebook(let accessToken):
            header[App.Header.authorization.rawValue] = "Bearer \(accessToken)"
        default:
            break
        }
        return header
    }
}
