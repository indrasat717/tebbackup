//
//  LoginModel.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 26/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

struct LoginModel: Decodable {
    let id: String
    let userId: Int
    let ttl: Int
    let created: String
    //let lastUpdated: String
    //let createUserId: Int
    //let updateUserId: Int
}
