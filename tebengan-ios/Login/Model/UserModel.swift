//
//  UserModel.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 3/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

struct UserModel: Decodable {
    let data: UserByIdModel
}

struct UserByIdModel: Decodable {
    let userById: UserDetailModel
}

struct UserDetailModel: Decodable {
    let nodeId: String?
    let genderId: Int?
    let name: String?
    let email: String?
    let familyName: String?
    let company: String?
    let position: String?
    let avatarUrl: String?
    let isEmailConfirmed: Int?
    let isPhoneVerified: Int?
    let phoneNumber: String?
    let description: String?
    let facebookId: String?
    let linkedinId: String?
}



