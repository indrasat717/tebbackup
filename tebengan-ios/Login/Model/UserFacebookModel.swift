//
//  UserFacebookModel.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 4/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

struct UserFacebookModel: Decodable {
    let email: String
    let first_name: String
    let last_name: String
    let picture: UserFacebookPictureModel
}

struct UserFacebookPictureModel: Decodable {
    let data: UserFacebookPictureDetailModel
}

struct UserFacebookPictureDetailModel: Decodable {
    let height: Int
    let width: Int
    let url: String
}

