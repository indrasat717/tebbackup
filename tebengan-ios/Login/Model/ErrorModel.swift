//
//  ErrorModel.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 26/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

struct ErrorModel: Decodable {
    let error: ErrorDetailModel
}

struct ErrorDetailModel: Decodable {
    let name: String
    let status: Int
    let message: String
    let statusCode: Int
    let code: String
}
