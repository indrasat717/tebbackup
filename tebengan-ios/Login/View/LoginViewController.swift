//
//  LoginViewController.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 5/2/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet private weak var facebookButtonView: UIViewDesignable!
    @IBOutlet private weak var linkedinButtonView: UIViewDesignable!
    @IBOutlet private weak var emailInputView: TextFieldInputView!
    @IBOutlet private weak var passwordInputView: TextFieldInputView!
    @IBOutlet private weak var forgetPasswordButton: UIButton!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var registerButton: UIButton!
    @IBOutlet private weak var dismissButton: UIBarButtonItem!
    
    private let viewModel = LoginViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
        initViewController()
    }
    
    @IBAction func dismissClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

private extension LoginViewController {
    func initViewModel() {
        viewModel.showLoadingClosure = { [weak self] (isLoading) in
            if isLoading {
                self?.addOverlayLoadingView()
            } else {
                self?.removeOverlayLoadingView()
            }
        }
        
        viewModel.showAlertClosure = { [weak self] (title, msg) in
            self?.presentAlert(title: title,
                               message: msg,
                               actions: [UIAlertAction(title: "OK", style: .default, handler: nil)],
                               preferredAction: nil)
        }
    }
    
    func initViewController() {
        facebookButtonView.tappable { [weak self] (_) in
            self?.viewModel.requestLoginFacebook()
        }
        
        linkedinButtonView.tappable { [weak self] (_) in
            self?.viewModel.requestLoginLinkedin()
        }
        
        loginButton.addTarget(self, action: #selector(loginMailClicked), for: .touchUpInside)
    }
    
    @objc func loginMailClicked() {
        viewModel.requestLoginMail(email: emailInputView.text, password: passwordInputView.text)
    }

}
