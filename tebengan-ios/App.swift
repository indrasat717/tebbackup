//
//  App.swift
//  tebengan-ios
//
//  Created by Avendi Sianipar on 4/3/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import Foundation

struct App {
    
    static let graphQLHost = URL(string: "")!
    static let graphFacebookHost = URL(string: "")!
    static let tebenganHost = URL(string: "")!

    enum Header: String {
        case contentType = "Content-Type"
        case authorization = "Authorization"
        case client = "client"
        case graphqlauth = "graphqlauth"
    }
    
    enum HeaderValue: String {
        case accept = "accept"
        case apps = "apps"
        case appsJson = "application/json"
        case tebenganGraphAuth = "tebenganGraph321!@#"
    }
}
