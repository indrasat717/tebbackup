//
//  SlideShow.swift
//  tebengan-ios
//
//  Created by Tebengan Developer on 02/01/20.
//  Copyright © 2020 Tebengan Developer. All rights reserved.
//

import UIKit
import ImageSlideshow

class SlideShow: XIBView {
    @IBOutlet private var slideShow: ImageSlideshow!
    
    let localSource = [BundleImageSource(imageString: "img2"),
                       BundleImageSource(imageString: "img3"),
                       BundleImageSource(imageString: "img1")]
    
    override func initView() {
        slideShow.setupSlideShow()
        slideShow.setPageIndicator()
        slideShow.setImageInputs(localSource)
    }    
}

extension ImageSlideshow {
    func setupSlideShow(){
        // Do any additional setup after loading the view.
        self.slideshowInterval = 5.0
        self.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        self.contentScaleMode = UIViewContentMode.scaleAspectFill

        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        self.activityIndicator = DefaultActivityIndicator()
        setPageIndicator()
    }
    func setPageIndicator(){
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.black
        self.pageIndicator = pageControl
        self.pageIndicatorPosition = PageIndicatorPosition(horizontal: .left(padding: 20), vertical: .bottom)
    }
}

